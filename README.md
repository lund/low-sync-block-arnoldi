# LowSyncBlockArnoldi

This repo comprises MATLAB files for low-synchronization block Arnoldi methods with generalized inner products.
The primary aim of this project is to explore speed, accuracy, and stability of a wide variety of methods.
There are four main axes required to specify an algorithm isotope:

* inner product: choice of block inner product (e.g., classical or global)
* skeleton: the inter-orthongalization routine applied between block vectors
* muscle: the intra-orthogonalization routine applied to an individual block vector
* modification: whether to use vanilla FOM (Full Orthogonalization Method), or a modified version, like GMRES.  (Radau-Arnoldi modifications currently not implemented)

Additional key parameter choices include maximum basis size, error/residual tolerance, and the maximum number of restart cycles allowed.

Regarding the organization of the code:

* `bfom` is the main algorithm driver, which switches between choice of inner product, modification, and skeleton-muscle combinations.
* `run_bfom` is the primary driver for benchmarking algorithms according to run time or for generating intermediate loss of orthogonality data, in order to study the stability of different algorithm isotopes
* `analyze_bfom` is a post-processing script overloaded to generate useful additional data from `run_bfom`
* `visualize_bfom` is an overloaded script for generating plots from `run_bfom`
* `tex_bfom` generates a LaTeX file summarizing the outputs from `analyze_bfom` and `visualize_bfom`

Have a look at `tests\tridiag.m` for a starter example.  If you want to test the algorithms on your own system, see `tests\suite_sparse.m` for a template.  That script is set up to run any matrix in the [Suite Sparse collection](https://sparse.tamu.edu/).

To reproduce the tests in the [paper](https://doi.org/10.1007/s11075-022-01437-1) (citation below), just run `tests/paper_script.m`

## Getting errors when I run `paper_script.m` on my machine

Exciting!  Either you've 1) found a bug or 2) you've demonstrated the issues highlighted in Section 4 of the [paper](https://doi.org/10.1007/s11075-022-01437-1).  If the error involves `svd` or `chol` and a complaint about computing with `NaN`s or a lack of positive definiteness, then try one of the following:

* Change the multithreading setting (`param.max_threads`).  Note that it is fixed to 16 for all the paper scripts.
* Run the script on a different operating system or MATLAB version.
* Reduce `m`.  You won't be able to reproduce the plots perfectly, but you might get close enough.

If none of these tricks helps, please write to me with the detailed specs of your system, the error message, and exactly which script with which parameters you tried to run.  The scripts have been tested on a few different machines running some combinations of Ubuntu 20.04 or Windows 10 with MATLAB 2019b and 2022a.  The `power_systems` example may not always converge, and `svd` errors are known to arise with Ubuntu kernels with version less than 5.15.0.

## Citation and further use

Please copy the license and cite the paper:
> Lund, Kathryn. Adaptively restarted block Krylov subspace methods with low-synchronization skeletons. Numerical Algorithms. 2022. [DOI: 10.1007/s11075-022-01437-1](https://doi.org/10.1007/s11075-022-01437-1)

## New features

Please [contact me](kathryn.d.lund@gmail.com), and I'll give you acces to make a branch.  I'll be working on new features myself every now and then, and I'm open to collaborations.

## This repository

The original version of this repo is hosted downstream on the [CSC Gitlab server](https://gitlab.mpi-magdeburg.mpg.de/lund/low-sync-block-arnoldi).  Any new developments will be made directly to this [Gitlab instance](https://gitlab.com/katlund/low-sync-block-arnoldi) and pushed downstream.

## Acknowledgements

Many thanks to [Matea Ugrica](https://www.mathos.unios.hr/index.php/26-homepage/odjel/kadrovi/homepages/410-matea-puvaca-s-home-page) and [Quirin Aumann](https://scholar.google.com/citations?user=q_vmX1UAAAAJ) for testing the code on various platforms.
