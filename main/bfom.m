function param = bfom(A, B, param)
% param = BFOM(A, B, param) computes a restarted BFOM approximation to the
% linear system AX = B.  A must be an n x n matrix, B an n x s block
% vector, where s << n.  The struct param takes the following fields.
%   - .conv_check: 'approx' or 'true'
%   - .conv_check_freq: 'cycle' or 'iteration' (how often to check for
%      convergence)
%   - .true_sol: true solution A\B
%   - .err_scale: scalar (usually norma of true solution or approximate)
%      by which to scale the error measure (which is the residual if
%      true_sol is not given)
%   - .inner_prod: inner products
%   - .max_cycles: maximum number of restart cycles
%   - .max_iterations: maximum basis size before restarting
%   - .max_threads: maximum number of CPU threads
%   - .mod: 'none'/'fom', 'harmonic'/'gmres' ('radau-lanczos' to be added
%      later)
%   - .musc: muscles
%   - .n: dimension of the operator A
%   - .num_runs: number of times to run each ip-skel-musc configuration
%      (average is taken for timings)
%   - .skel: skeletons
%   - .store_loss_ortho: 0 (don't store); 1 (do store)
%   - .store_cond_BAV: 0 (don't store); 1 (do store)
%   - .tol: desired accuracy
%   - .verbose: 0 (bare minimum print-outs); 1 (just error); 2 (error and
%      loss of orthogonality [LOO])
%
% param is updated with various intermediate and final outputs throughout
% the computation.  Possible outputs of interest include
%   - .Acount: total number of A-calls
%   - .alg_config: a cell of strings denoting the executed algorithm
%      configurations
%   - .approx_res: residual computed as 2-norm of cospatial factors
%   - .approx_sol: the final approximate solution
%   - .basis_eval_count: total number of times the basis V is applied
%   - .cond_BAV: a cell containing iteration-wise computations of
%      cond([B A*V]), where V is the Krylov basis
%   - .true_err = error computed directly from the true_sol
%   - .loss_ortho: a cell containing iteration-wise loss of orthogonality
%      data for each cycle
%   - .nan_flag: boolean for whether a NaN has been detected
%   - .run_times: a cell of vectors containing run times per iteration;
%      each cell corresponds to a cycle
%   - .sync_count = total number of synchronization points
%
% Defaults are set by PARAM_INIT_BFOM.  See also TIME_BFOM, STAB_BFOM, or
% TRIDIAG for possible BFOM configurations.
%
% Notes about timings: Computations for true error, loss of orthogonality,
% and condition numbers are excluded from timings, in order to simulate a
% method checking convergence only with the approximate residual.  Note
% that the approximate residual is always computed and stored, even when
% conv_check == 'true'.  Verbose outputs are likewise excluded from the
% timings.

%%
% Initialize timer
tic;
rt = 0;

% Populate missing fields
param = param_init_bfom(param);
param.nan_flag = false;

% Extract block size and other reused parameters
basis_size = param.max_iterations;
conv_check = param.conv_check;
conv_check_freq = param.conv_check_freq;
ip = lower(param.inner_prod);
max_cycles = param.max_cycles;
mod = lower(param.mod);
s = param.s;
store_cond_BAV = param.store_cond_BAV;
store_loss_ortho = param.store_loss_ortho;
verbose = param.verbose;

% Save algorithm configuration
param.alg_config = sprintf('%s-%s(%s)-%s',...
    param.inner_prod, param.skel, param.musc, param.mod);

% Set up verbose print-outs
rt = rt + toc;
if verbose
    fprintf('\n%s\n', param.alg_config)
end
if verbose == 1
    switch conv_check
        case 'approx'
            fprintf('       apx_res    \n');
        case 'true'
            fprintf('       apx_res    |  true_err   \n');
    end
elseif verbose == 2
    switch conv_check
        case 'approx'
            fprintf('       apx_res    |     LOO      \n');
        case 'true'
            fprintf('       apx_res    |  true_err   |     LOO      \n');
    end
end
tic;

% Flag to compute and store Beta for the first cycle
param.first_cycle = true;

% Compute norm(B) for relative residual
res_scale = block_norm(B, param, A);

% Initialize output storage for all cycles
approx_res_cell = cell(1,max_cycles);
run_times_cell = cell(1,max_cycles);
rt = rt + toc;
true_err_cell = cell(1,max_cycles);
if store_loss_ortho
    loss_ortho_cell = cell(1,max_cycles);
end
if store_cond_BAV
    cond_BAV_cell = cell(1,max_cycles);
end
tic;

% Initialize convergence flag
param.flag = 0;

% Initialize storage for each cycle
approx_res = zeros(1,basis_size);
run_times = zeros(1,basis_size);
rt = rt + toc;
true_err = zeros(1,basis_size);
if store_cond_BAV
    cond_BAV = zeros(1,basis_size);
end
if store_loss_ortho
    loss_ortho = zeros(1,basis_size);
end
tic;

% Initialize starting vector
start_vec = B;
for k = 1:max_cycles
    % Always replace previously computed basis
    param.new_cycle = true;

    % Make verbose pretty
    rt = rt + toc;
    if verbose == 1
        switch conv_check
            case 'approx'
                fprintf('(%3.0d)----------------\n', k);
            case 'true'
                fprintf('(%3.0d)----------------------------\n', k);
        end
    elseif verbose == 2
        switch conv_check
            case 'approx'
                fprintf('(%3.0d)----------------------------\n', k);
            case 'true'
                fprintf('(%3.0d)-------------------------------------------\n', k);
        end
    end
    tic;

    % Compute next cycle
    for j = 1:basis_size
        % Compute next basis vector
        param = block_arnoldi_step(A, start_vec, param);
        
        % Adaptive restart heuristics:
        %   1) Check for non-finite entries in Hessenberg
        %   2) Reduce basis size
        %   3) Do not compute a new approximation; instead revert to last
        %       saved iteration
        %   4) Move on to next cycle
        if param.nan_flag
        % If too stringent, then try  `if any(~isfinite(param.Hnext))` instead
            if k == 1 && j == 1
                % Failure on first iteration of first cycle --> give up
                param.flag = 4;
                print_flag(param);
                return
            else
                % Revert to back-up
                param = param_prev;
    
                % Reduce basis size for next cycle
                basis_size = j-1;
                break
            end
        end
        
        % Switch block unit vector, indices, and I based on inner product
        switch ip
            case 'gl'
                E1 = block_unit(j,1,1);
                Ej = block_unit(j,1,j);
                ind = j;
                rt = rt + toc;
                if verbose == 2 || store_loss_ortho
                    I = eye(j+1);
                end
                tic;
    
            case 'cl'
                E1 = block_unit(j*s,s,1);
                Ej = block_unit(j*s,s,j);
                ind = j*s-s+1:j*s;
                rt = rt + toc;
                if verbose == 2 || store_loss_ortho
                    I = eye(j*s+s);
                end
                tic;
        end
    
        % Compute modification
        switch lower(mod)
            case {'harmonic', 'gmres'}
                M = (param.Hshort' \ Ej) * param.Hnext' * param.Hnext;
            case {'none', 'fom'}
                M = 0 * E1;
        end
        
        % Compute and store solution and cospatial factor Xim
        if k == 1
            Xim = (param.Hshort + M * Ej') \ (E1 * param.Beta);
            [approx_sol, param] = basis_eval(param.Vshort, Xim, param);
        else
            Xim = (param.Hshort + M * Ej') \ (E1 * param.Beta * Xim_end);
            [Dm, param] = basis_eval(param.Vshort, Xim, param);

            % Temporarily update solution
            approx_sol = param.approx_sol + Dm;
        end
    
        % Compute and store approximate residual
        Vmplus = [param.Vshort param.Vnext];
        [start_vec, param] = basis_eval(Vmplus, [M; -param.Hnext], param);
        switch mod
            case {'harmonic', 'gmres'}
                approx_res(j) = block_norm([M; -param.Hnext] * Xim(ind,:),...
                    param, A) / res_scale;
            case {'none', 'fom'}
                approx_res(j) = block_norm(-param.Hnext * Xim(ind,:),...
                    param, A) / res_scale;
        end
        
        % Print approximate residual
        rt = rt + toc;
        if verbose
            fprintf('%3.0d:', j);
            fprintf('  %2.4e  ', approx_res(j));
        end
        tic;
        
        % Switch tol_check
        switch conv_check
            case 'approx'
                tol_check = approx_res(j);
                
            case 'true'
                rt = rt + toc;
                true_err(j) = ...
                    block_norm(approx_sol - param.true_sol, param, A) /...
                    param.err_scale;
                tol_check = true_err(j);
                
                if verbose
                    fprintf('|  %2.4e  ', true_err(j));
                end
                tic;
        end
        
        % Compute, store, and print LOO
        rt = rt + toc;
        if verbose == 2 || store_loss_ortho
            loo = norm(I - inner_prod(Vmplus, Vmplus, param));
            if verbose == 2
                fprintf('|  %2.4e\n', loo);
            elseif store_loss_ortho
                loss_ortho(j) = loo;
            end
        end
        if verbose == 1
            fprintf('\n')
        end

        % Compute and store cond([B AV])
        if store_cond_BAV
            cond_BAV(j) = cond([B matvec(A, Vmplus, param)]);
        end
        tic;

        % Convergence check
        if tol_check < param.tol
            param.flag = 1;
            print_flag(param);
    
            % Save outputs
            approx_res_cell{k} = approx_res(1:j);
            param.approx_res = approx_res_cell(1:k);
    
            rt = rt + toc;
            true_err_cell{k} = true_err(1:j);
            param.true_err = true_err_cell(1:k);
    
            if store_loss_ortho
                loss_ortho_cell{k} = loss_ortho(1:j);
                param.loss_ortho = loss_ortho_cell(1:k);
            end
            if store_cond_BAV
                cond_BAV_cell{k} = cond_BAV(1:j);
                param.cond_BAV = cond_BAV_cell(1:k);
            end
            tic;

            param.approx_sol = approx_sol;

            run_times(j) = rt + toc;
            run_times_cell{k} = run_times(1:j);
            param.run_times = run_times_cell(1:k);
            return
        end
        
        % Save back-up param in case of breakdown
        param_prev = param;

        % Save run times and reset timer
        run_times(j) = rt + toc;
        tic;
        rt = 0;
    end

    % Update solution and cospatial factor (approx_sol and Cm should only
    % accumulate per cycle, not per iteration in each cycle)
    param.approx_sol = approx_sol;
    Xim_end = Xim(ind,:);

    % Check if we've exhausted the basis size
    if basis_size == 0
        param.flag = 4;
        print_flag(param)

        run_times(j) = run_times(j) + rt + toc;
        run_times_cell{k} = run_times(1:j);
        param.run_times = run_times_cell(1:k);
        return
    end

    % Save outputs from this cycle
    approx_res_cell{k} = approx_res(1:basis_size);

    rt = rt + toc;
    true_err_cell{k} = true_err(1:basis_size);

    if store_loss_ortho
        loss_ortho_cell{k} = loss_ortho(1:basis_size);
    end
    if store_cond_BAV
        cond_BAV_cell{k} = cond_BAV(1:basis_size);
    end
    tic;

    % Save back-up param in case of breakdown
    param_prev = param;

    % Save run times and reset timer
    run_times(j) = run_times(j) + rt + toc;
    run_times_cell{k} = run_times(1:basis_size);
    tic;
    rt = 0;
end
% If this point is reached, then cycles have been exhausted with no
% convergence
if j < param.max_iterations
    % Then we ran out of restart chances, but we didn't completely deplete
    % the basis size
    param.flag = 5;
else
    param.flag = 3;
end
print_flag(param);
param.approx_res = approx_res_cell;
param.true_err = true_err_cell;
rt = rt + toc;
if store_loss_ortho
    param.loss_ortho = loss_ortho_cell;
end
if store_cond_BAV
    param.cond_BAV = cond_BAV_cell;
end
tic;
run_times(j) = run_times(j) + rt + toc;
run_times_cell{k} = run_times(1:j);
param.run_times = run_times_cell;
end