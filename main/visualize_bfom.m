function visualize_bfom(run_data, param)
% VISUALIZE_BFOM(run_data, param) turns run_data into pretty plots.
% run_data should be a struct returned by ANALYZE_BFOM.

%%
fprintf('Please wait while plots are being generated.\nThis may take some time.\n')

% Set up directories for saving
if ~isfield(param,'save_str')
    param.save_str = '../tests/results/user_test';
    fprintf('Your data will be saved at tests/results/user_test.\n')
end
if ~exist(param.save_str, 'dir')
    mkdir(param.save_str)
end

% Extract reusable quantities
alg_config = run_data.alg_config;
alg_config_pretty = run_data.alg_config_pretty;
num_alg = length(alg_config);

if isfield(run_data, 'run_times_cum')
    time_flag = true;
end
if isfield(run_data, 'loss_ortho')
    stab_flag = true;
end

if time_flag
    % Build matrix to plot as bar chart
    measure_str = {'run_times_cum', 'Acounts', 'basis_eval_counts', 'sync_counts'};
    legend_str = {'Total Run Time', '$A$ Count', '$V$ Count', 'Sync Count'};
    num_meas = length(measure_str);
    measure_mat = zeros(num_alg, num_meas);
    for i = 1:num_meas
        measure = run_data.(measure_str{i});
        measure = measure / max(measure);
        if i == 1
            % Sort by first measure
            [~, ind] = sort(measure, 'ascend');
            alg_config_sorted = categorical(alg_config_pretty(ind));
        end
        measure_mat(:,i) = measure(ind);
    end
    
    % Generate bar chart
    fig = figure('Visible','off','Position',[50 300 900 600]);
    ax = gca;
    bar(ax, measure_mat);
    legend(legend_str, 'Location', 'Best', 'Interpreter', 'latex');
    ylabel('measure scaled by series maximum')
    
    % Touch up aesthetics
    set(ax, 'XTickLabel', alg_config_sorted,...
        'XTickLabelRotation', 45, ...
        'XTick', 1:num_alg,...
        'TickLabelInterpreter', 'none',...
        'FontSize', 14)
    save_str = sprintf('%s/run_times_bar_chart', param.save_str);
    savefig(fig, save_str);
    saveas(fig, save_str, 'epsc');
    close all;
end
if stab_flag
    % Plot error, residual, LOO, cond([B AV]) for all algorithm isotopes
    % relative to iteration index
    cmap = lines(4)*.9;
    fig = figure('Visible','off');
    for a = 1:num_alg
        ax = gca;
        
        hold on;
        plot(ax, run_data.approx_res{a}, '-', 'Color', cmap(1,:),...
            'LineWidth', 1.5)
        plot(ax, run_data.true_err{a}, '-.', 'Color', cmap(2,:),...
            'LineWidth', 1.5)
        plot(ax, run_data.cond_BAV{a}, '--', 'Color', cmap(3,:),...
            'LineWidth', 1.5)
        plot(ax, run_data.loss_ortho{a}, '.:', 'Color', cmap(4,:),...
            'LineWidth', 1.5)
        grid on
        xlabel(ax, 'Iteration Index')
        axis tight
        if isempty(run_data.true_err{a})
            legend('residual', 'cond([B AV]', 'LOO', 'Location', 'Best')
        else
            legend('residual', 'error', 'cond([B AV]', 'LOO', 'Location', 'Best')
        end
        title(alg_config_pretty{a},'Interpreter','none')
        set(ax, 'YScale', 'log', 'XMinorGrid', 'off', 'FontSize', 16)
        hold off;
        
        save_str = sprintf('%s/%s_iter', param.save_str, alg_config{a});
        savefig(fig, save_str);
        saveas(fig, save_str, 'epsc');
        
        clf;
    end
    close all;
end
fprintf('To see figures, use the openfig command with ''visible'' flag.\n')
end