function param = block_arnoldi_step(A, X, param)
% param = BLOCK_ARNOLDI_STEP(A, V, param) switches between different
% skeleton implementations of block Arnoldi.  Note that X is not necessary
% for continuing a basis.  Basis continuation proceeds only if
% param.new_cycle = true.

%%
skel = lower(param.skel);
switch skel
    case 'bmgs'
        param = bmgs_arnoldi_step(A, X, param);

    case {'bmgs_svl', 'bmgs_lts'}
        param = bmgs_svl_arnoldi_step(A, X, param);

    case {'bmgs_cwy', 'bmgs_icwy'}
        param = bmgs_cwy_arnoldi_step(A, X, param);

    case {'bmgs_pm_cwy', 'bmgs_pm_icwy'}
        param = bmgs_pm_arnoldi_step(A, X, param);

    case {'bcgs_pio', 'bcgs_pip'}
        param = bcgs_p_arnoldi_step(A, X, param);

    case 'bcgs_iro_ls'
        param = bcgs_iro_ls_arnoldi_step(A, X, param);

    otherwise
        error('%s is not a viable skeleton.')
end
end