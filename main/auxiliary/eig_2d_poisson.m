function ev = eig_2d_poisson(N)
% ev = EIG_2D_POISSON(N) computes the eigenvalues of the two-dimensional
% discretized Poisson operator with N nodes in each direction.  Formulas
% taken from Chapter 6 of J. Demmel, "Applied Numerical Linear Algebra",
% 1997.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
j = 1:N;                                                                    % N = number of nodes in 1D
x = 2*(1 - cos(j*pi/(N+1)));                                                % 1D eigenvalues
[X,Y] = meshgrid(x,x);
ev = reshape(X + Y,N^2,1);
end
