function N = block_norm(X, param, A)
% N = BLOCK_NORM(X, param, A) computes a scalar norm for the block vector
% X.

%%
% Defaults
if nargin == 1
    param.norm = 'fro';
elseif nargin == 2
    if ~isfield(param,'norm')
        param.norm = 'fro';
    end
end

switch param.norm
    case 'A-fro'                                                            % A-weighted Frobenius norm
        N = sqrt(real(trace(X'*( matvec(A,X) ) ) ) );
    case {'fro', 'global-no-scale'}                                         % Frobenius norm; default
        N = norm(X,'fro');
    case {'gl', 'global'}                                                   % global norm (i.e., scaled Frobenius)
        s = size(X,2);
        N = norm(X,'fro')/sqrt(s);
    case 'harmonic'                                                         % A'*A-weighted norm
        N = norm(matvec(A,X),'fro');
    case 'max-col'                                                          % max over the 2-norms of each column
        s = size(X,2);
        N = zeros(1,s);
        for i = 1:s
            N(i) = norm(X(:,i),2);
        end
        N = max(N);
    case {2,'2'}                                                            % matrix 2-norm
        N = norm(X,2);
    case {1,'1'}                                                            % matrix 1-norm
        N = norm(X,1);
    case {inf,Inf,'inf','Inf'}                                              % matrix inf-norm
        N = norm(X,Inf);
end
