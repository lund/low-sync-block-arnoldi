function alg_str = alg_string(alg_str)
% ALG_STRING(alg_str) converts the algorithm identifier given by alg into a
% more legible string for plots

%%
num_alg = length(alg_str);
alg_str = upper(alg_str);
for a = 1:num_alg
    str = alg_str{a};
    str = strrep(str, 'CL', 'cl');
    str = strrep(str, 'GL', 'gl');
    str = strrep(str, '_FREE', '');
    str = strrep(str, '_IRO_', 'I+');
    str = strrep(str, '_IRO', 'I+');
    str = strrep(str, '_SRO', 'S+');
    str = strrep(str, '_RO_', '+');
    str = strrep(str, 'RO_', '+');
    str = strrep(str, '_RO', '+');
    str = strrep(str, 'RO', '+');
    str = strrep(str, 'CHOLQR', 'CholQR');
    str = strrep(str, 'ITER_', 'Iter');
    str = strrep(str, 'SH_', 'Sh');
    str = strrep(str, 'HOUSEQR', 'HouseQR');
    str = strrep(str, '_PINV', '-pinv');
    str = strrep(str, '_', '-');

    alg_str{a} = str;
end