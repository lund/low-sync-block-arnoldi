function [param, modified] = param_init_bfom(param)
% [param, modified] = PARAM_INIT_BFOM(param) generates and/or checks the
% input parameter struct.
%
% param = PARAM_INIT_BFOM returns a default setting
% param = PARAM_INIT_BFOM(param) returns a corrected parameter setting

%%
modified = 0;

if ~nargin
    param = struct;
    modified = 1;
end

if ~isfield(param, 'verbose')
    param.verbose = 1;
    disp('Warning: .verbose not specified, set to 1.');
    modified = 1;
end

if ~isfield(param, 'true_sol')
    if ~isfield(param,'conv_check')
        param.conv_check = 'approx';
        modified = 1;
    else
        if strcmp(param.conv_check, 'true')
            param.conv_check = 'approx';
            modified = 1;
        end
    end
    if param.verbose && modified == 1
        disp('Warning: .true not specified, so .conv_check set to ''approx''.');
    end
else
    if ~isfield(param, 'conv_check')
        param.conv_check = 'true';
        if param.verbose
            disp('Warning: .true_sol provided, so .conv_check set to ''true''.');
        end
        modified = 1;
    end
end

if ~isfield(param, 'conv_check_freq')
    param.conv_check_freq = 'cycle';
    if param.verbose
        disp('Warning: .true provided, so .conv_check set to ''true''.');
    end
    modified = 1;
end

if ~isfield(param, 'err_scale') && isfield(param, 'true_sol')
    param.err_scale = norm(param.true,'fro');
    if param.verbose
        disp('Warning: .true provided, but not .err_scale, so .err_scale set to norm(param.true,''fro'').');
    end
    modified = 1;
end

if ~isfield(param,'inner_prod')
    param.inner_prod = 'gl';
    if param.verbose
        disp('Warning: .inner_prod not specified, set to ''gl''.');
    end
    modified = 1;
end

if ~isfield(param,'max_cycles')
    param.max_cycles = 100;
    if param.verbose
        disp('Warning: .max_cycles not specified, set to 100.');
    end
    modified = 1;
end

if ~isfield(param,'max_threads')
    param.max_threads = 4;
    if param.verbose
        disp('Warning: .max_threads not specified, set to 4.');
    end
    modified = 1;
end

if ~isfield(param,'mod')
    param.mod = 'none';
    if param.verbose
        disp('Warning: .mod not specified, set to ''none''.');
    end
    modified = 1;
end

if ~isfield(param,'norm')
    if isfield(param,'hermitian')
        if param.hermitian
            param.norm = 'A-fro';
            if param.verbose
                disp('Warning: .norm not specified, set to ''A-fro''.');
            end
            modified = 1;
        else
            param.norm = 'fro';
            if param.verbose
                disp('Warning: .norm not specified, set to ''fro''.');
            end
            modified = 1;
        end
    else
        param.norm = 'fro';
        if param.verbose
            disp('Warning: .norm not specified, set to ''fro''.');
        end
        modified = 1;
    end
end

if ~isfield(param, 'max_iterations')
    param.max_iterations = 25;
    if param.verbose
        disp('Warning: .max_iterations not specified, set to 25.');
    end
    modified = 1;
end

if ~isfield(param, 'skel')
    param.skel = 'bmgs';
    if param.verbose
        disp('Warning: .skel not specified, set to ''bmgs''.');
    end
    modified = 1;
end

if strcmp(param.skel, 'bmgs_svl')
    param.musc = 'mgs_svl';
    if param.verbose
        disp('Warning: .musc set to ''mgs_svl'' to match skeleton.');
    end
    modified = 1;
end

if strcmp(param.skel, 'bmgs_lts')
    param.musc = 'mgs_lts';
    if param.verbose
        disp('Warning: .musc set to ''mgs_lts'' to match skeleton.');
    end
    modified = 1;
end

if ~isfield(param, 'musc')
    switch param.inner_prod
        case 'cl'
            param.musc = 'houseqr';
            if param.verbose
                disp('Warning: .musc not specified, set to ''houseqr''.');
            end
            modified = 1;
        case 'gl'
            param.musc = 'gl';
            if param.verbose
                disp('Warning: .musc not specified, set to ''gl''.');
            end
            modified = 1;
    end
end

if ~isfield(param, 'store_cond_BAV')
    param.store_cond_BAV = false;
    if param.verbose
        disp('Warning: .store_cond_BAV not specified, set to false.');
    end
    modified = 1;
end

if ~isfield(param, 'store_loss_ortho')
    param.store_loss_ortho = false;
    if param.verbose
        disp('Warning: .store_loss_ortho not specified, set to false.');
    end
    modified = 1;
end

if strcmp(param.inner_prod,'gl')
    if strcmpi(param.skel, 'bcgs_pio')
        param.skel = 'bcgs_pip';
        if param.verbose
            fprintf('Warning: %s not a valid skeleton for global inner product, set to BCGS_PIP instead.',...
                param.skel);
        end
        modified = 1;
    end
    if ~strcmpi(param.musc,'gl')
        param.musc = 'gl';
        if param.verbose
            fprintf('Warning: %s not a valid muscle for global inner product, set to ''gl'' instead.',...
                param.musc);
        end
        modified = 1;
    end
end

if ~isfield(param, 'tol')
    param.tol = 1e-6;
    if param.verbose
        disp('Warning: .tol not specified, set to 1e-6.');
    end
    modified = 1;
end
end
