function [C, param] = inner_prod(X, Y, param)
% [C, param] = INNER_PROD(X, Y, param) switches between different inner
% product paradigms, determined by param.musc.

%%
% Extract block size and muscle
s = param.s;

% Standardize muscle
str = lower(param.musc);

% Initialize block inner product
XY = X'*Y;

% Initialize or increase sync counter
if ~isfield(param, 'sync_count')
    param.sync_count = 1;
else
    param.sync_count = param.sync_count + 1;
end

% Switch muscle
switch str
    case {'cl', 'cgs_p', 'cgs_iro_ls', 'mgs','mgs_svl',...
            'mgs_lts', 'mgs_icwy', 'mgs_cwy', 'houseqr', 'cholqr',...
            'cholqr_ro', 'sh_cholqr_roro', 'iter_cholqr'}
        C = XY;
        
    case {'gl'}
        dim = size(XY) / s;
        p = dim(1); q = dim(2);
        C = zeros(dim);
        for i = 1:p
            ii = (i-1)*s+1:i*s;
            for j = 1:q
                jj = (j-1)*s+1:j*s;
                C(i,j) = trace(XY(ii,jj));
            end
        end
        C = C / s;
        
    otherwise
        error('%s is not a viable muscle option', str);
end
end