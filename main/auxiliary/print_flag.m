function print_flag(param)
% PRINT_FLAG(param) is a subroutine for decoding param.flag.

%%
alg_config = param.alg_config;
switch param.flag
    case 0
        fprintf('%s: Flag unchanged.  Check code for bugs.\n', alg_config)
    case 1
        fprintf('%s: Method converged to desired tolerance.\n', alg_config);
    case 2
        fprintf('%s: Method stagnated before reaching desired tolerance.\n', alg_config)
    case 3
        fprintf('%s: Maximum number of cycles reached before convergence.\n', alg_config)
    case 4
        fprintf('%s: Unable to avoid non-finite Hessenberg.\n', alg_config)
    case 5
        fprintf('%s: Unable to avoid non-finite Hessenberg before running out of cycle allowance.\n', alg_config)
end
end