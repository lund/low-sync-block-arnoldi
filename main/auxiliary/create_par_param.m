function par_param = create_par_param(ip, mod, skel, musc)
% Auxiliary function for creating parallelization-friendly param structs
num_ip = length(ip);
num_mod = length(mod);
num_skel = length(skel);
num_musc = length(musc);

par_param = [];
par_param.ip = {};
par_param.mod = {};
par_param.skel = {};
par_param.musc = {};
for i = 1:num_ip
    for mo = 1:num_mod
        for sk = 1:num_skel
            for mu = 1:num_musc
                switch ip{i}
                    case 'gl'
                        switch skel{sk}
                            case 'bcgs_pio'
                                % Global inner product not compatible with
                                % bcgs_pio
                                continue
                            otherwise
                                if mu == 1
                                    par_param.ip{end+1} = ip{i};
                                    par_param.mod{end+1} = mod{mo};
                                    par_param.skel{end+1} = skel{sk};
        
                                    % Force muscle to be 'gl'
                                    par_param.musc{end+1} = 'gl';
                                else
                                    % No need to re-run 'gl', only one
                                    % approved muscle
                                    break
                                end
                        end
                    case 'cl'
                        par_param.ip{end+1} = ip{i};
                        par_param.mod{end+1} = mod{mo};
                        par_param.skel{end+1} = skel{sk};
                        par_param.musc{end+1} = musc{mu};
                end
            end
        end
    end
end
end