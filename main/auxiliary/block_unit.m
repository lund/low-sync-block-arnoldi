function v = block_unit(ms,s,k)
% v = block_unit(ms,s) returns an ms x s block vector with identity in the
% kth s x s block
%%
% E1 is common enough to set this as a default
if nargin == 2
    k = 1;
end

if s == 1
    v = zeros(ms,s);
    v(k) = 1;
elseif s > 1
    if round(ms/s) == ms/s
        v = zeros(ms,s);
        v((k-1)*s+1:k*s,:) = eye(s);
    else
        error('ms is not a multiple of s.')
    end
end
end