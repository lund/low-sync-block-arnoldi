function [out, param] = basis_eval(V, X, param)
% [out, param] = basis_eval(V, X, param) returns the action of a basis V on
% a matrix X. Block size is given by param.s.
%
% To compute the action of the adjoint of V on X, use inner_prod(V,X).

%%
% Extract s for legibility
s = param.s;

% Initialize or update counter
if ~isfield(param, 'basis_eval_count')
    param.basis_eval_count = 1;
else
    param.basis_eval_count = param.basis_eval_count + 1;
end

switch param.inner_prod
    case 'cl'
        % True matrix-matrix multiplication
        out = V * X;
        
    case 'gl'
        % No matrix-matrix multiplication necessary; just a sum of
        % scaled matrices.
        [m,p] = size(X);
        [n, ms] = size(V);
        if ms / s ~= m
            error('The first dimension of X must match the size of the basis.')
        end
        out = zeros(n,p*s);
        for i = 1:p
            ii = (i-1)*s+1:i*s;
            out_ii = 0;
            for j = 1:m
                jj = (j-1)*s+1:j*s;
                out_ii = out_ii + V(:,jj) * X(j,i);
            end
            out(:,ii) = out_ii;
        end
end
end