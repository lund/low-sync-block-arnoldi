function [Y, param] = matvec(A, X, param)
% [Y, param] = MATVEC(A, X, param) computes the action of an operator A on
% X and updates the counter for applications of A.  A must be given as a
% matrix array or a function handle.

%%
if isnumeric(A)
    Y = A*X;
else
    Y = A(X);
end
if isfield(param, 'Acount')
    param.Acount = param.Acount + 1;
else
    param.Acount = 1;
end
end