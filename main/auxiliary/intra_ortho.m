function [Q, R, T, param] = intra_ortho(X, param)
% [Q, R, T, param] = INTRA_ORTHO(X, param) is a wrapper function for
% switching between intra-orthonormalization algorithms specified by
% param.musc.

%%
addpath(genpath('muscles'))

% Initialize
if ~isfield(param, 'sync_count')
    param.sync_count = 0;
end

switch lower(param.musc)
    case {'cgs_p'}
        [Q, R, sync_count] = cgs_p(X);
        
    case {'cgs_iro_ls'}
        [Q, R, sync_count] = cgs_iro_ls(X);
        
%--------------------------------------------------------------------------
    case {'mgs'}
        [Q, R, sync_count] = mgs(X);
        
    case {'mgs_svl'}
        [Q, R, T, sync_count] = mgs_svl(X);
        
    case {'mgs_lts'}
        [Q, R, T, sync_count] = mgs_lts(X);
        
%--------------------------------------------------------------------------
    case {'houseqr'}
        % Without access to the built-in function, hard to know-- for the
        % moment, we pretend HouseQR is a stand-in for AllReduceQR
        [Q, R] = qr(X,0);
        sync_count = 1;
        
%--------------------------------------------------------------------------
    case {'cholqr'}
        [Q, R] = cholqr(X);
        sync_count = 1;
        
%--------------------------------------------------------------------------
    case {'global', 'gl'}
        s = size(X,2);
        R = norm(X,'fro') / sqrt(s);
        Q = X / R;
        sync_count = 1;
        
    otherwise
        error('%s is not a viable muscle option', musc);
end

% Return identity for T when not otherwise generated
if ~exist('T', 'var')
    T = eye(size(R));
end

% Update sync count
param.sync_count = param.sync_count + sync_count;
end