function tex_bfom(run_data, param)
% TEX_BFOM(run_data, param) generates a basic TeX file for easily viewing
% run_data and its plots.

%%
fprintf('Generating TeX file for figures. Should be quick.\n')

% Set up directories for saving
if ~isfield(param,'save_str')
    param.save_str = '../tests/results/user_test';
    fprintf('Your data will be saved at tests/results/user_test.\n')
end
if ~exist(param.save_str, 'dir')
    mkdir(param.save_str)
end

% Extract fields
alg_config = run_data.alg_config;
alg_config_pretty = run_data.alg_config_pretty;
num_alg = length(alg_config);
Acounts = run_data.Acounts;
basis_eval_counts = run_data.basis_eval_counts;
run_times_cum = run_data.run_times_cum;
speed_up = run_data.speed_up;
sync_counts = run_data.sync_counts;
total_cycles = run_data.total_cycles;
total_iterations = run_data.total_iterations;

% Open new file
save_str = sprintf('%s/figures.tex', param.save_str);
fID = fopen(save_str,'w');

% Preamble
fprintf(fID, '\\documentclass[10pt]{article}\n');
fprintf(fID, '\\usepackage{booktabs, makecell} %% for column headers\n');
fprintf(fID, '\\usepackage{datetime2}\n');
fprintf(fID, '\\usepackage{epsfig, epstopdf}\n');
fprintf(fID, '\\usepackage{float}\n');
fprintf(fID, '\\usepackage{geometry, graphicx}\n');
fprintf(fID, '\\geometry{a4paper}\n');
fprintf(fID, '\n');
fprintf(fID, '\\title{Results}\n');
fprintf(fID, '\\author{}\n');
fprintf(fID, '\\date{\\DTMnow}\n');
fprintf(fID, '\n');

% Begin document
fprintf(fID, '\\begin{document}\n');
fprintf(fID, '\\maketitle\n');
fprintf(fID, '\n');

% Run details
fprintf(fID, '\\section{Run Details}\n');
fprintf(fID, '\\begin{itemize}\n');
test_id = replace(param.test_id, '_', '\_');
fprintf(fID, '\t\\item Problem ID: \\texttt{%s}\n', test_id);
fprintf(fID, '\t\\item Problem size ($n$): $%d$\n', param.n);
fprintf(fID, '\t\\item Block size ($s$): $%d$\n', param.s);
fprintf(fID, '\t\\item Max basis size ($m$): $%d$\n', param.max_iterations);
fprintf(fID, '\t\\item Max cycles: $%d$\n', param.max_cycles);
fprintf(fID, '\t\\item Norm: \\texttt{''%s''}\n', param.norm);
fprintf(fID, '\t\\item Number of runs per method: $%d$\n', param.num_runs);
fprintf(fID, '\t\\item Tolerance: $%1.2e$\n', param.tol);
fprintf(fID, '\\end{itemize}\n');

% Finish Line Benchmarks
fprintf(fID, '\\section{Finish Line Benchmarks}\n');
fprintf(fID, '\\begin{table}[H]\n');
fprintf(fID, '\t\\begin{center}\n');
fprintf(fID, '\t\t\\begin{tabular}{c|c|c|c|c|c|c|c}\n');
fprintf(fID, '\t\t\t\\toprule\n');
fprintf(fID, '\t\t\t\\thead{Config}	& \\thead{Time (s)}	& \\thead{\\%%\\\\Speed-up}	& \\thead{Cycle\\\\Count}	& \\thead{Iteration\\\\Count}	& \\thead{$A$\\\\Count} & \\thead{$V$\\\\Count}	& \\thead{Sync\\\\Count} \\\\\n');
fprintf(fID, '\t\t\t\\midrule\n');
for a = 1:num_alg
    fprintf(fID, '\t\t\t%s      ', alg_config_pretty{a});
    fprintf(fID, '& %2.2e       ', run_times_cum(a));
    fprintf(fID, '& %2.2f       ', speed_up(a));
    fprintf(fID, '& %d          ', total_cycles(a));
    fprintf(fID, '& %d          ', total_iterations(a));
    fprintf(fID, '& %d          ', Acounts(a));
    fprintf(fID, '& %d          ', basis_eval_counts(a));
    fprintf(fID, '& %d          ', sync_counts(a));
    if a ~= num_alg
        fprintf(fID, '\\\\\\hline\n');
    else
        fprintf(fID, '\n');
    end
end
fprintf(fID, '\t\t\\end{tabular}\n');
fprintf(fID, '\t\\end{center}\n');
fprintf(fID, '\\end{table}\n');
fprintf(fID, '\n');
fprintf(fID, '\\begin{figure}[H]\n');
fprintf(fID, '\t\\resizebox{.9\\textwidth}{!}{\\includegraphics{run_times_bar_chart.eps}}\n');
fprintf(fID, '\\end{figure}\n');
fprintf(fID, '\n');

conv_str = {'iter','time'};
for i = 1
    % time graphs don't appear to be so interesting
    if i == 1
        fprintf(fID, '\\section{Convergence history, relative iteration index}\n');
    elseif i == 2
        fprintf(fID, '\\section{Convergence history, relative time elapsed}\n');
    end
    for a = 1:num_alg
        if mod(a,2)
            if a ~= num_alg
                fprintf(fID, '\\begin{figure}[H]\n');
                fprintf(fID, '\t\\begin{tabular}{cc}\n');
                fprintf(fID, '\t\t\\resizebox{.45\\textwidth}{!}{\\includegraphics{%s_%s.eps}} &\n',alg_config{a}, conv_str{i});
            else % then a is the last one
                fprintf(fID, '\\begin{figure}[H]\n');
                fprintf(fID, '\t\\resizebox{.45\\textwidth}{!}{\\includegraphics{%s_%s.eps}}\n',alg_config{a}, conv_str{i});
                fprintf(fID, '\\end{figure}\n');
                fprintf(fID, '\n');
            end
        else
            fprintf(fID, '\t\t\\resizebox{.45\\textwidth}{!}{\\includegraphics{%s_%s.eps}} \\\\\n', alg_config{a}, conv_str{i});
            fprintf(fID, '\t\\end{tabular}\n');
            fprintf(fID, '\\end{figure}\n');
            fprintf(fID, '\n');
        end
    end
end

% End document
fprintf(fID, '\\end{document}\n');

fclose(fID);
end