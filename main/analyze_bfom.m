function run_data = analyze_bfom(run_data, param)
% run_data = ANALYZE_BFOM(run_data, param) turns the run_data struct into a
% user-readable table and prints it to screen with algorithms ranked by
% speed.

%%
% Set up directories for saving
if ~isfield(param,'save_str')
    param.save_str = '../tests/results/user_test';
    fprintf('Your data will be saved at tests/results/user_test.\n')
end
if ~exist(param.save_str, 'dir')
    mkdir(param.save_str)
end

% Clean up alg_config for plots and tables
alg_config = run_data.alg_config;
alg_config_pretty = alg_string(alg_config);
num_alg = length(alg_config_pretty);

if isfield(run_data, 'run_times_cum')
    % Extract data
    Acounts = run_data.Acounts;
    basis_eval_counts = run_data.basis_eval_counts;
    run_times_cum = run_data.run_times_cum;
    sync_counts = run_data.sync_counts;
    total_cycles = run_data.total_cycles;
    total_iterations = run_data.total_iterations;

    % Calculate ratio between each method and slowest method
    slowest_run_time = max(run_times_cum);
    speed_up = round((1 - run_times_cum / slowest_run_time)*100,2);
    run_data.speed_up = speed_up;

    % Calculate average runtime per iteration
    time_per_it = run_times_cum ./ total_iterations;
    run_data.time_per_it = time_per_it;

    % Calculate time per A-call
    time_per_A = run_times_cum ./ Acounts;
    run_data.time_per_A = time_per_A;

    % Calculate time per basis_eval
    time_per_basis_eval = run_times_cum ./ basis_eval_counts;
    run_data.time_per_basis_eval = time_per_basis_eval;

    % Calculate time per sync
    time_per_sync = run_times_cum ./ sync_counts;
    run_data.time_per_sync = time_per_sync;

    % Calculate ratio between sync counts and A-calls
    sync_per_A = round(sync_counts ./ Acounts);
    run_data.sync_per_A = sync_per_A;

    % Print table to screen
    VarNames = {'Config', 'Time (s)', '% Speed-up', '# Cycles',...
        '# Its', 'Time per It (s)', 'A-calls', 'Time per A-call (s)',...
        'V-calls', 'Time per V-call (s)',...
        'sync-count', 'Time per sync (s)', 'sync:A'};
    run_data_table = table(alg_config_pretty,...
        sprintfc('%2.4e',run_times_cum), speed_up,...
        total_cycles,total_iterations, sprintfc('%2.4e',time_per_it),...
        Acounts, sprintfc('%2.4e',time_per_A),...
        basis_eval_counts, sprintfc('%2.4e',time_per_basis_eval),...
        sync_counts, sprintfc('%2.4e',time_per_sync),...
        sync_per_A,...
        'VariableNames', VarNames);
    run_data.table = sortrows(run_data_table, '% Speed-up');
    disp(run_data.table);

    % Reassign data and sort according to speed-up
    [run_data.speed_up, ind] = sort(speed_up, 'ascend');
    run_data.alg_config = alg_config(ind);
    run_data.alg_config_pretty = alg_config_pretty(ind);
    run_data.Acounts = Acounts(ind);
    run_data.basis_eval_counts = basis_eval_counts(ind);
    run_data.run_times_cum = run_times_cum(ind);
    run_data.run_times = run_data.run_times(ind);
    run_data.sync_counts = sync_counts(ind);
    run_data.total_cycles = total_cycles(ind);
    run_data.total_iterations = total_iterations(ind);

end
if isfield(run_data, 'loss_ortho')
    true_err = run_data.true_err;
    approx_res = run_data.approx_res;
    cond_BAV = run_data.cond_BAV;
    loss_ortho = run_data.loss_ortho;
    
    % Put all computed values into a single vector
    for a = 1:num_alg
        true_err{a} = cell2mat(true_err{a});
        approx_res{a} = cell2mat(approx_res{a});
        cond_BAV{a} = cell2mat(cond_BAV{a});
        loss_ortho{a} = cell2mat(loss_ortho{a});
    end
    
    if exist('ind', 'var')
        run_data.true_err = true_err(ind);
        run_data.approx_res = approx_res(ind);
        run_data.cond_BAV = cond_BAV(ind);
        run_data.loss_ortho = loss_ortho(ind);
    else
        run_data.true_err = true_err;
        run_data.approx_res = approx_res;
        run_data.cond_BAV = cond_BAV;
        run_data.loss_ortho = loss_ortho;
    end
end

% Save param and run data
save_str = sprintf('%s/run_data.mat',param.save_str);
save(save_str,'param', 'run_data');