function param = bcgs_p_arnoldi_step(A, B, param)
% param = BCGS_P_ARNOLDI_STEP(A, B, param) computes the next step of a
% BCGS-PIO-Arnoldi or BCGS-PIP-Arnoldi basis for A and B with a given inner
% product (param.inner_prod) and muscle (param.musc).  When
% `param.new_cycle = true`, the basis is continued from a previously stored
% basis in param. param.skel indicates whether PIO or PIP is used.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract reused values to avoid calling param repeatedly
ip = param.inner_prod;

if param.new_cycle % Start the basis from scratch
    % Initialize storage and indices
    V = zeros(n, 2*s);
    k = 1;
    kk = 1:s;
    ks = k*s;

    % Compute first basis vector and Beta
    if param.first_cycle
        [V(:,kk), Beta, ~, param] = intra_ortho(B, param);
        param.Beta = Beta;
    else
        V(:,kk) = B;
    end
    
    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);
    
    % Run a step of BCGS_P
    [V(:,kk+s), R_diag, S, param] = bcgs_p_step(V(:,1:ks), W, param);

    switch ip
        case 'cl'
            % Initialize storage
            H = zeros(ks,s);
            
            % Update and store H
            H(kk+s,kk) = R_diag;
            H(1:ks,kk) = S;
            param.Hnext = H(end-s+1:end,end-s+1:end);
            param.Hshort = H(1:end-s,:);

        case 'gl'
            % Initialize storage
            H = zeros(k+1,k);

            % Update and store H
            H(k+1,k) = R_diag;
            H(1:k,k) = S;
            param.Hnext = H(end,end);
            param.Hshort = H(1:end-1,:);
    end
    
    param.new_cycle = false;
    
else % Continue the basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.Hshort,1)/s;
            H = [param.Hshort zeros(k*s,s);...
                zeros(s,k*s-s) param.Hnext zeros(s)];

        case 'gl'
            k = size(param.Hshort,1);
            H = [param.Hshort zeros(k,1);
                zeros(1,k-1) param.Hnext 0];
    end
    V = [param.Vshort param.Vnext zeros(n, s)];
    
    % Initialize indices
    k = k+1;
    ks = k*s;
    kk = ks-s+1:ks;

    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);

    % Run a step of BCGS_P
    [V(:,kk+s), R_diag, S, param] = bcgs_p_step(V(:,1:ks), W, param);
    
    % Update and store H
    switch ip
        case 'cl'
            H(kk+s,kk) = R_diag;
            H(1:ks,kk) = S;
            param.Hnext = H(end-s+1:end,end-s+1:end);
            param.Hshort = H(1:end-s,:);

        case 'gl'
            H(k+1,k) = R_diag;
            H(1:k,k) = S;
            param.Hnext = H(end,end);
            param.Hshort = H(1:end-1,:);
    end
end

param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end

function [Q, R_diag, S, param] = bcgs_p_step(QQ, W, param)
% Extract dimension for readability
s = param.s;

switch param.inner_prod
    case 'cl'
        switch param.skel
            case 'bcgs_pio'
                [S, param] = inner_prod(QQ, W, param);
                [~, tmp, ~, param] =...
                    intra_ortho([W zeros(size(W)); zeros(size(S)) S], param);
                tmp = tmp' * tmp;
                Om = tmp(1:s,1:s) - tmp(end-s+1:end, end-s+1:end);
        
            case 'bcgs_pip'
                [tmp, param] = inner_prod([QQ W], W, param);
                S = tmp(1:end-s,:);
                Om = tmp(end-s+1:end,:) - S'*S;
        end
        [R_diag, param.nan_flag] = chol_nan(Om);
        
    case 'gl'        
        % Only BCGS-PIP variant is valid (and it reduces more intuitively
        % to CGS-P)
        [tmp, param] = inner_prod([QQ W], W, param);
        S = tmp(1:end-1);
        Om = tmp(end) - S'*S;
        if Om < 0
            % Triggers dependent routines to stop a few iterations sooner
            R_diag = NaN;
        else
            R_diag = sqrt(Om);
        end
end

[tmp_vec, param] = basis_eval(QQ, S, param);
W = W - tmp_vec;
Q = W / R_diag;
end