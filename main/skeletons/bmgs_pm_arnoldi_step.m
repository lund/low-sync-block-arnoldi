function param = bmgs_pm_arnoldi_step(A, B, param)
% param = BMGS_PM_ARNOLDI_STEP(A, B, param) computes the next step of a
% BMGS-PM-CWY-Arnoldi or BMGS-PM-ICWY-Arnoldi basis for A and B with the
% inner product specified by param.inner_prod.  When `param.new_cycle =
% true`, the basis is continued from a previously stored basis in param.
% param.skel indicates whether CWY or ICWY is used.
%
% Based on Algorithm9.m from
% https://github.com/kswirydo/LowSynchGMRESAlgorithms

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract parameters for readability
ip = param.inner_prod;

if param.new_cycle
    % Initalize storage, block indices, and compute first basis vector
    k = 1;
    kk = 1:s;
    ks = k*s;
    V = zeros(n, 2*s);
    switch ip
        case 'cl'
            R = zeros(2*s);
            T = eye(2*s);
            [V(:,kk), R(kk,kk), T(kk,kk), param] = intra_ortho(B, param);
        case 'gl'
            R = zeros(2);
            T = eye(2);
            [V(:,kk), R(k,k), T(k,k), param] = intra_ortho(B, param);
    end

    % Compute next vector to orthonormalize
    [W, param] = matvec(A, V(:,kk), param);

    % Run a step of BMGS_PM_CWY (k = 2), reorthonormalize, and assign H
    [R, T, V, param] = bmgs_pm_step(R, T, V, W, k, param);
    switch ip
        case 'cl'
            param.Beta = R(kk,kk);
            param.Hnext = R(ks+1:ks+s,kk+s);
            param.Hshort = R(1:ks,s+1:ks+s);
        case 'gl'
            param.Beta = R(k,k);
            param.Hnext = R(k+1,k+1);
            param.Hshort = R(1:k,2:k+1);
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

else % Continue basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.T,1)/s;
            R = [param.R zeros(k*s,s); zeros(s,k*s+s)];
            T = [param.T zeros(k*s,s); zeros(s,k*s) eye(s)];    % eye(s) crucial!
    
        case 'gl'
            k = size(param.T,1);
            R = [param.R zeros(k,1); zeros(1,k+1)];
            T = [param.T zeros(k,1); zeros(1,k) 1];    % 1 crucial!
    end
    V = [param.Vshort param.Vnext zeros(n,s)];

    % Update block indices
    ks = k*s;
    kk = ks-s+1:ks;
    
    % Compute next vector to orthonormalize
    [W, param] = matvec(A, V(:,kk), param);

    % Run a step of BMGS_PM_CWY, reorthonormalize, and assign H
    [R, T, V, param] = bmgs_pm_step(R, T, V, W, k, param);
    switch ip
        case 'cl'
            param.Hnext = R(ks+1:ks+s,kk+s);
            param.Hshort = R(1:ks,s+1:ks+s);
        case 'gl'
            param.Hnext = R(k+1,k+1);
            param.Hshort = R(1:k,2:k+1);
    end
end

% Assign outputs
param.R = R;
param.T = T;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end

function [R, T, V, param] = bmgs_pm_step(R, T, V, W, k, param)
% Extract parameter for readability
ip = param.inner_prod;

% Set up block indices
s = param.s;
s1 = 1:s;
s2 = s+1:2*s;
ks = k*s;
kk = ks-s+1:ks;

% Compute inner product
[tmp_prod, param] = inner_prod(V(:,1:ks), [V(:,ks-s+1:ks) W], param);
switch ip
    case 'cl'
        % Label chunks of tmp for readability
        Y = tmp_prod(1:ks-s,s1);
        Z = tmp_prod(1:ks-s,s2);
        Om = tmp_prod(ks-s+1:ks,s1);

        % Pythagorean trick for R diagonal
        [R_diag, param.nan_flag] = chol_nan(Om);

        % Switch how T is updated and R via T based on skeleton choice
        P = R_diag' \ tmp_prod(ks-s+1:ks,s2);
        switch lower(param.skel)
            case 'bmgs_pm_cwy'
                % Update error cushion T
                T(1:ks-s,kk) = -T(1:ks-s, 1:ks-s) * (Y / R_diag);

                % Update R entries
                R(kk,kk+s) = R(kk,kk+s) / R_diag;
                R(1:ks,kk+s) = T(1:ks, 1:ks)' * [Z; P];

            case 'bmgs_pm_icwy'
                % Update error cushion T
                T(1:ks-s,kk+s) = Y / R_diag;

                % Update R entries
                R(kk,kk+s) = R(kk,kk+s) / R_diag;
                R(1:ks,kk+s) = T(1:ks,1:ks)' \ [Z; P];
        end
        S = R(1:ks,kk+s);

    case 'gl'
        % Label chunks of tmp for readability
        Y = tmp_prod(1:k-1,1);
        Z = tmp_prod(1:k-1,2);
        Om = tmp_prod(k,1);
        
        % Pythagorean trick for R diagonal
        R_diag = sqrt(Om);

        % Switch how T is updated and R via T based on skeleton choice
        P = tmp_prod(k,2) / R_diag;
        switch lower(param.skel)
            case 'bmgs_pm_cwy'
                % Update error cushion T
                T(1:k-1,k) = -T(1:k-1, 1:k-1) * (Y / R_diag);

                % Update R entries
                R(k,k+1) = R(k,k+1) / R_diag;
                R(1:k,k+1) = T(1:k, 1:k)' * [Z; P];

            case 'bmgs_pm_icwy'
                % Update error cushion T
                T(1:k-1,k) = Y / R_diag;

                % Update R entries
                R(k,k+1) = R(k,k+1) / R_diag;
                R(1:k,k+1) = T(1:k, 1:k)' \ [Z; P];
        end
        S = R(1:k,k+1);
end

% Update V
V(:,kk) = V(:,kk) / R_diag;
[tmp_vec, param] = basis_eval(V(:,1:ks), S, param);
U = W - tmp_vec;

% Normalize V
[V(:,kk+s), R_next, ~, param] = intra_ortho(U, param);
switch ip
    case 'cl'
        R(kk+s,kk+s) = R_next;
    case 'gl'
        R(k+1,k+1) = R_next;
end
end