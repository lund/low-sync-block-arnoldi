function param = bcgs_iro_ls_arnoldi_step_original(A, B, param)
% param = BCGS_IRO_LS_ARNOLDI_STEP_ORIGINAL(A, B, param) computes the next
% step of a BCGSI+LS-Arnoldi basis for A and B with the inner product
% specified by param.inner_prod. When `param.new_cycle = true`, the basis
% is continued from a previously stored basis in param.
%
% This was the first attempt at converting BCGS_IRO_LS into an Arnoldi
% routine and employs an unstable Hessenberg reconstruction.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract parameters for readability
ip = param.inner_prod;

% Set up block indices for temporary matrices (cl only)
s1 = 1:s;
s2 = s+1:2*s;

if param.new_cycle % Start new basis from scratch
    % Initialize storage and auxiliary block vector
    switch ip
        case 'cl'
            H = zeros(2*s,s);
            R = zeros(2*s,3*s);
        case 'gl'
            H = zeros(2,1);
            R = zeros(2,3);
    end
    U = B;
    V = zeros(n, 2*s);
    
    for k = 2:3
        % Increment block index
        kk = (k-1)*s+1:k*s;
        ks = k*s;
    
        % Compute next vector to orthonormalize
        [W, param] = matvec(A, U, param);
    
        % Compute temporary quantities -- the only sync point!
        if k == 2
            [tmp, param] = inner_prod(U, [U W], param);
            switch ip
                case 'cl'
                    Om = tmp(:, s1);
                    P = tmp(:, s2);
                case 'gl'
                    Om = tmp(1);
                    P = tmp(2);
            end
    
            % Pythagorean trick for R diagonals
            switch ip
                case 'cl'
                    [R_diag, param.nan_flag] = chol_nan(Om);

                    % Assign finished entries of RR
                    R(kk-s, kk-s) = R_diag;
                    R(kk-s, kk) = R_diag' \ P;

                case 'gl'
                    % Pythagorean trick for R diagonal
                    R_diag = sqrt(Om);

                    % Assign finished entries of RR
                    R(k-1, k-1) = R_diag;
                    R(k-1, k) = R_diag' \ P;
            end
        
            % Finish normalizing V
            V(:,kk-s) = U / R_diag;

            % Assign and store Beta
            if param.first_cycle
                param.Beta = R_diag;
            end
                
            % Set up auxiliary block vector for next iteration
            switch ip
                case 'cl'
                    S = R(1:ks-s, kk);

                case 'gl'
                    S = R(1:k-1, k);
            end

            [tmp_vec, param] = basis_eval(V(:, 1:ks-s), S, param);
            U = W - tmp_vec;
            
        elseif k == 3
            % Run a step of BCGS_IRO_LS
            [R, U, V, param] = bcgs_iro_ls_step(R, U, V, W, k, param);
        end
    end
    
    % Reconstruct H
    switch ip
        case 'cl'
            H(1:ks-s,kk-2*s) = R(1:ks-s,kk-s) / R(1:s,1:s);
    
        case 'gl'
            H(1:k-1,k-2) = R(1:k-1,k-1) / R(1,1);
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

else % Continue basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.Hshort,1)/s;
            H = [param.Hshort zeros(k*s,s);...
                zeros(s,(k-1)*s) param.Hnext zeros(s);...
                zeros(s,(k+1)*s)];
            R = [param.R zeros((k+1)*s,s);...
                zeros(s,(k+3)*s)];
        case 'gl'
            k = size(param.Hshort,1);
            H = [param.Hshort zeros(k,1);...
                zeros(1,k-1) param.Hnext 0;...
                zeros(1,k+1)];
            R = [param.R zeros(k+1,1);...
                zeros(1,k+3)];
    end
    U = param.U;
    V = [param.Vshort param.Vnext zeros(n,s)];
    
    % Increment block index
    k = k+3;
    ks = k*s;
    kk = ks-s+1:ks;
    
    % Compute next vector to orthonormalize
    [W, param] = matvec(A, U, param);
    
    % Run a step of BCGS_IRO_LS
    [R, U, V, param] = bcgs_iro_ls_step(R, U, V, W, k, param);
    
    % Reconstruct H
    switch ip
        case 'cl'
            col = R(1:ks-s,kk-s) / R(1:s,1:s);
            jj = 1:s;
            for j = 2:k-2
                jj = jj + s;
                col = col / H(jj,jj-s);
            end
            H(1:ks-s,kk-2*s) = col;
    
        case 'gl'
            col = R(1:k-1,k-1) / R(1,1);
            for j = 2:k-2
                col = col / H(j,j-1);
            end
            H(1:k-1,k-2) = col;
    end
end

% Assign outputs
switch ip
    case 'cl'
        param.Hnext = H(ks-2*s+1:ks-s,ks-3*s+1:ks-2*s);
        param.Hshort = H(1:ks-2*s,1:ks-2*s);

    case 'gl'
        param.Hnext = H(k-1,k-2);
        param.Hshort = H(1:k-2,1:k-2);
        
end
param.R = R;
param.U = U;
param.Vnext = V(:,ks-2*s+1:ks-s);
param.Vshort = V(:,1:ks-2*s);
end

function [R, U, V, param] = bcgs_iro_ls_step(R, U, V, W, k, param)
% Extract parameter for readability
ip = param.inner_prod;

% Set up block indices
s = param.s;
s1 = 1:s;
s2 = s+1:2*s;
ks = k*s;
kk = ks-s+1:ks;

% Compute temporary quantities -- the only sync point!
[YZ, param] = inner_prod([V(:,1:ks-2*s) U], [U W], param);
switch ip
    case 'cl'
        Y = YZ(:,s1);
        Z = YZ(:,s2);
        tmp = YZ(end-s+1:end,:) - Y(1:end-s,:)' * YZ(1:end-s,:);    % local inner product
        Om = tmp(:,s1);
        P = tmp(:,s2);

        % Pythagorean trick for R diagonal
        [R_diag, param.nan_flag] = chol_nan(Om);

        % Assign finished entries of R
        R(kk-s, kk-s) = R_diag;
        R(kk-s, kk) = R_diag' \ P;
        R(1:ks-2*s, kk-s) = R(1:ks-2*s, kk-s) + Y(1:ks-2*s,:);
        R(1:ks-2*s, kk) = Z(1:ks-2*s,:);

        % Set up for U
        indU1 = 1:ks-s;
        indU2 = kk;

        % Set up for normalizing V
        indV = 1:ks-2*s;

    case 'gl'
        Y = YZ(:,1);
        Z = YZ(:,2);
        tmp = YZ(end,:) - Y(1:end-1)' * YZ(1:end-1,:);    % local inner product
        Om = tmp(1);
        P = tmp(2);

        % Pythagorean trick for R diagonal
        R_diag = sqrt(Om);

        % Assign finished entries of R
        R(k-1, k-1) = R_diag;
        R(k-1, k) = P / R_diag;
        R(1:k-2, k-1) = R(1:k-2, k-1) + Y(1:k-2,:);
        R(1:k-2, k) = Z(1:k-2,:);

        % Set up for U
        indU1 = 1:k-1;
        indU2 = k;

        % Set up for normalizing V
        indV = 1:k-2;
end
% Finish normalizing last V
[tmp_vec, param] = basis_eval(V(:,1:ks-2*s), Y(indV,:), param);
V(:,kk-s) = (U - tmp_vec) / R_diag;

% Finish setting up auxiliary vector U
[tmp_vec, param] = basis_eval(V(:, 1:ks-s), R(indU1,indU2), param);
U = W - tmp_vec;
end