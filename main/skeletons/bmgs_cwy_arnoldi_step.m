function param = bmgs_cwy_arnoldi_step(A, B, param)
% param = BMGS_CWY_ARNOLDI_STEP(A, B, param) computes the next step of a
% BMGS-CWY-Arnoldi or BMGS-ICWY-Arnoldi basis for A and B with the inner
% product specified by param.inner_prod.  When `param.new_cycle = true`,
% the basis is continued from a previously stored basis in param.
% param.skel indicates whether CWY or ICWY is used.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract parameters for readability
ip = param.inner_prod;

if param.new_cycle
    % Initalize storage, starting vector, and block indices
    switch ip
        case 'cl'
            H = zeros(2*s,s);
            T = eye(3*s);
        case 'gl'
            H = zeros(2,1);
            T = eye(3);
    end
    V = zeros(n, 2*s);

    % First step
    if param.first_cycle
        [V(:,1:s), Beta, ~, param] = intra_ortho(B, param);
        param.Beta = Beta;
    else
        V(:,1:s) = B;
    end

    % Initialize auxiliary vector
    U = V(:,1:s);

    for k = 1:2
        % Compute next vector to orthonormalize
        [W, param] = matvec(A, U, param);

        if k == 1
            % Compute temporary quantities -- the only sync point!
            [tmp, param] = inner_prod(U, W, param);
            
            switch ip
                case 'cl'
                    H(1:s,1:s) = tmp;
                case 'gl'
                    H(1,1) = tmp;
            end

            % Set up auxiliary block vector for next iteration
            [tmp_vec, param] = basis_eval(V(:,1:s), tmp, param);
            U = W - tmp_vec;

        elseif k == 2
            % Run a step of BMGS_CWY
            [H, J, T, U, V, param] = bmgs_cwy_step(H, T, U, V, W, k, param);
        end
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

    % Assign outputs
    switch ip
        case 'cl'
            ks = k*s;
            param.Hnext = H(ks-s+1:ks,ks-2*s+1:ks-s);
            param.Hshort = H(1:ks-s,1:ks-s);
    
        case 'gl'
            param.Hnext = H(k,k-1);
            param.Hshort = H(1:k-1,1:k-1);
    end

else % Continue basis
    
    % Extract current basis and add J column to Hessenberg
    switch ip
        case 'cl'
            k = size(param.T,1)/s;
            H = [[param.Hshort; zeros(s,(k-3)*s) param.Hnext] param.J;...
                zeros(s,(k-1)*s)];
            T = [param.T zeros(k*s,s); zeros(s,k*s) eye(s)];    % eye(s) crucial!

        case 'gl'
            k = size(param.T,1);
            H = [[param.Hshort; zeros(1,k-3) param.Hnext] param.J;...
                zeros(1,k-1)];
            T = [param.T zeros(k,1); zeros(1,k) 1];    % 1 crucial!
    end
    U = param.U;
    V = [param.Vshort param.Vnext zeros(n,s)];
    
    % Compute next vector to orthonormalize
    [W, param] = matvec(A, U, param);

    % Run a step of BMGS_CWY
    [H, J, T, U, V, param] = bmgs_cwy_step(H, T, U, V, W, k, param);

    % Assign outputs
    switch ip
        case 'cl'
            ks = k*s;
            param.Hnext = H(ks-s+1:ks,ks-2*s+1:ks-s);
            param.Hshort = H(1:ks-s,1:ks-s);
    
        case 'gl'
            param.Hnext = H(k,k-1);
            param.Hshort = H(1:k-1,1:k-1);
    end
end

% Assign outputs
param.J = J;
param.T = T;
param.U = U;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end

function [H, J, T, U, V, param] = bmgs_cwy_step(H, T, U, V, W, k, param)
% Extract parameter for readability
ip = param.inner_prod;

% Set up block indices
s = param.s;
s1 = 1:s;
s2 = s+1:2*s;
ks = k*s;
kk = ks-s+1:ks;

% Compute temporary quantities -- the only sync point!
[tmp_prod, param] = inner_prod([V(:,1:ks-s) U], [U W], param);
switch ip
    case 'cl'
        Y = tmp_prod(1:ks-s,s1);
        Z = tmp_prod(1:ks-s,s2);
        Om = tmp_prod(ks-s+1:ks,s1);

        % Pythagorean trick for R diagonal
        [R_diag, param.nan_flag] = chol_nan(Om);

        % Update H and T
        H(kk,kk-s) = R_diag;
        P = R_diag' \ tmp_prod(ks-s+1:ks,s2);
        switch lower(param.skel)
            case 'bmgs_cwy'
                % Update error cushion T
                T(1:ks-s,kk) = -T(1:ks-s,1:ks-s) * (Y / R_diag);

                % Update next column of H
                J = T(1:ks,1:ks)' * ([Z; P] / R_diag);

            case 'bmgs_icwy'
                % Update error cushion T
                T(1:ks-s,kk) = Y / R_diag;

                % Update next column of H
                J = T(1:ks,1:ks)' \ ([Z; P] / R_diag);
        end

    case 'gl'
        Y = tmp_prod(1:k-1,1);
        Z = tmp_prod(1:k-1,2);
        Om = tmp_prod(k,1);

        % Pythagorean trick for R diagonal
        R_diag = sqrt(Om);

        % Update H and T
        H(k,k-1) = R_diag;
        P = tmp_prod(k,2) / R_diag;
        switch lower(param.skel)
            case 'bmgs_cwy'
                % Update error cushion T
                T(1:k-1,k) = -T(1:k-1,1:k-1) * (Y / R_diag);

                % Update next column of H
                J = T(1:k,1:k)' * ([Z; P] / R_diag);

            case 'bmgs_icwy'
                % Update error cushion T
                T(1:k-1,k) = Y / R_diag;

                % Update next column of H
                J = T(1:k,1:k)' \ ([Z; P] / R_diag);
        end
end

% Orthonormalize next basis vector
V(:,kk) = U / R_diag;

% Set up auxiliary block vector for next iteration
[tmp_vec, param] = basis_eval(V(:,1:ks), J, param);
U = W / R_diag - tmp_vec;
end