function param = bmgs_cwy_arnoldi_step_original(A, B, param)
% param = BMGS_CWY_ARNOLDI_STEP_ORIGINAL(A, B, param) computes the next
% step of a BMGS-CWY-Arnoldi or BMGS-ICWY-Arnoldi basis for A and B with
% the inner product specified by param.inner_prod.  When `param.new_cycle =
% true`, the basis is continued from a previously stored basis in param.
% param.skel indicates whether CWY or ICWY is used.
%
% This was the first attempt at converting BMGS-CWY and BMGS-ICWY into an
% Arnoldi routine and employs an unstable Hessenberg reconstruction.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract parameters for readability
ip = param.inner_prod;

% Set up block indices for temporary matrices (cl only)
s1 = 1:s;
s2 = s+1:2*s;

if param.new_cycle
    % Need to start off with two steps (see also BCGS_IRO_LS).  Simply
    % normalizing V_{k-1} as in SLA2020 doesn't seem like the natural
    % extension of CWY/ICWY methods, as it introduces a second sync.
    % Conversely, this version remains single-reduce, except for the first
    % block column (where we need two syncs to get Beta, H_{11}, and
    % H_{21}).

    % Initalize storage, starting vector, and block indices
    switch ip
        case 'cl'
            R = zeros(2*s,3*s);
            T = eye(3*s);
        case 'gl'
            R = zeros(2,3);
            T = eye(3);
    end
    U = B;
    V = zeros(n, 2*s);

    for k = 1:2
        % Initialize block indices
        kk = (k-1)*s+1:k*s;
        ks = k*s;

        % Compute next vector to orthonormalize
        [W, param] = matvec(A, U, param);

        % Compute temporary quantities -- the only synmax_iterationsc point!
        if k == 1
            [tmp, param] = inner_prod(U, [U W], param);
            switch ip
                case 'cl'
                    Om = tmp(:, s1);
                    P = tmp(:, s2);                    
        
                    % Pythagorean trick for R diagonal
                    [R_diag, param.nan_flag] = chol_nan(Om);

                    % Update R entries
                    R(kk,kk) = R_diag;

                    % Switch how R is updated via T based on skeleton choice
                    switch lower(param.skel)
                        case 'bmgs_cwy'
                            R(1:ks,kk+s) = T(1:ks,1:ks)' * (R_diag' \ P);

                        case 'bmgs_icwy'
                            R(1:ks,kk+s) = T(1:ks,1:ks)' \ (R_diag' \ P);
                    end
            
                case 'gl'
                    Om = tmp(1);
                    P = tmp(2);

                    % Pythagorean trick for R diagonal
                    R_diag = sqrt(Om);

                    % Update R entries
                    R(k,k) = R_diag;

                    % Switch how R is updated via T based on skeleton choice
                    switch lower(param.skel)
                        case 'bmgs_cwy'
                            R(1:k,k+1) = T(1:k,1:k)' * (R_diag' \ P);

                        case 'bmgs_icwy'
                            R(1:k,k+1) = T(1:k,1:k)' \ (R_diag' \ P);
                    end     
            end
    
            % Assign and store Beta
            if param.first_cycle
                param.Beta = R_diag;
            end
            
            % Orthonormalize next basis vector
            V(:,kk) = U / R_diag;
            
            % Set up auxiliary block vector for next iteration
            switch ip
                case 'cl'
                    S = R(1:ks,kk+s);
                    
                case 'gl'
                    S = R(1:k,k+1);
            end
            [tmp_vec, param] = basis_eval(V(:,1:ks), S, param);
            U = W - tmp_vec;

        elseif k == 2
            % Run a step of BMGS_CWY
            [V, R, T, U, param] = bmgs_cwy_step(V, R, T, U, W, k, param);
        end
    end
    
    % Assign and reconstruct H
    switch ip
        case 'cl'
            H = R(1:ks,kk) / R(1:s,1:s);
            param.Hnext = H(ks-s+1:ks,ks-2*s+1:ks-s);
            param.Hshort = H(1:ks-s,1:ks-s);
        case 'gl'
            H = R(1:k,k) / R(1,1);
            param.Hnext = H(k,k-1);
            param.Hshort = H(1:k-1,1:k-1);
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

else % Continue basis
    
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.T,1)/s;
            H = [param.Hshort zeros((k-2)*s,s);...
                zeros(s,(k-3)*s) param.Hnext zeros(s);...
                zeros(s,(k-1)*s)];
            R = [param.R zeros((k-1)*s,s); zeros(s,(k+1)*s)];
            T = [param.T zeros(k*s,s); zeros(s,k*s) eye(s)];    % eye(s) crucial!
    
            % Update block indices
            ks = k*s;
            kk = ks-s+1:ks;
        case 'gl'
            k = size(param.T,1);
            H = [param.Hshort zeros(k-2,1);...
                zeros(1,k-3) param.Hnext 0;...
                zeros(1,k-1)];
            R = [param.R zeros(k-1,1); zeros(1,k+1)];
            T = [param.T zeros(k,1); zeros(1,k) 1];    % 1 crucial!
    end
    U = param.U;
    V = [param.Vshort param.Vnext zeros(n,s)];
    
    % Compute next vector to orthonormalize
    [W, param] = matvec(A, U, param);

    % Run a step of BMGS_CWY
    [V, R, T, U, param] = bmgs_cwy_step(V, R, T, U, W, k, param);

    % Reconstruct and assign H
    switch ip
        case 'cl'
            col = R(1:ks,kk) / R(1:s,1:s);
            jj = 1:s;
            for j = 2:k-1
                jj = jj + s;
                col = col / H(jj,jj-s);
            end
            H(1:ks,kk-s) = col;
            param.Hnext = H(ks-s+1:ks,ks-2*s+1:ks-s);
            param.Hshort = H(1:ks-s,1:ks-s);
    
        case 'gl'
            col = R(1:k,k) / R(1,1);
            for j = 2:k-1
                col = col / H(j,j-1);
            end
            H(1:k,k-1) = col;
            param.Hnext = H(k,k-1);
            param.Hshort = H(1:k-1,1:k-1);
    end
end

% Assign outputs
param.R = R;
param.T = T;
param.U = U;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end

function [V, R, T, U, param] = bmgs_cwy_step(V, R, T, U, W, k, param)
% Extract parameter for readability
ip = param.inner_prod;

% Set up block indices
s = param.s;
s1 = 1:s;
s2 = s+1:2*s;
ks = k*s;
kk = ks-s+1:ks;

% Compute temporary quantities -- the only sync point!
[tmp, param] = inner_prod([V(:,1:ks-s) U], [U W], param);
switch ip
    case 'cl'
        Y = tmp(1:ks-s,s1);
        Z = tmp(1:ks-s,s2);
        Om = tmp(ks-s+1:ks,s1);
        P = tmp(ks-s+1:ks,s2);

        % Pythagorean trick for R diagonal
        [R_diag, param.nan_flag] = chol_nan(Om);

        % Assign R diagonal
        R(kk,kk) = R_diag;

        % Switch how T is updated and R via T based on skeleton choice
        switch lower(param.skel)
            case 'bmgs_cwy'
                % Update error cushion T
                T(1:ks-s,kk) = -T(1:ks-s,1:ks-s) * (Y / R_diag);

                % Update R entries
                R(1:ks,kk+s) = T(1:ks,1:ks)' * [Z; R_diag' \ P];

            case 'bmgs_icwy'
                % Update error cushion T
                T(1:ks-s,kk) = Y / R_diag;

                % Update R entries
                R(1:ks,kk+s) = T(1:ks,1:ks)' \ [Z; R_diag' \ P];
        end
        S = R(1:ks,kk+s);

    case 'gl'
        Y = tmp(1:k-1,1);
        Z = tmp(1:k-1,2);
        Om = tmp(k,1);
        P = tmp(k,2);

        % Pythagorean trick for R diagonal
        R_diag = sqrt(Om);

        % Assign R diagonal
        R(k,k) = R_diag;

        % Switch how T is updated and R via T based on skeleton choice
        switch lower(param.skel)
            case 'bmgs_cwy'
                % Update error cushion T
                T(1:k-1,k) = -T(1:k-1,1:k-1) * (Y / R_diag);

                % Update R entries
                R(1:k,k+1) = T(1:k,1:k)' * [Z; R_diag' \ P];

            case 'bmgs_icwy'
                % Update error cushion T
                T(1:k-1,k) = Y / R_diag;

                % Update R entries
                R(1:k,k+1) = T(1:k,1:k)' \ [Z; R_diag' \ P];
        end
        S = R(1:k,k+1);
end

% Orthonormalize next basis vector
V(:,kk) = U / R_diag;

% Set up auxiliary block vector for next iteration
[tmp_vec, param] = basis_eval(V(:,1:ks), S, param);
U = W - tmp_vec;
end