function param = bmgs_svl_arnoldi_step(A, B, param)
% param = BMGS_SVL_ARNOLDI_STEP(A, B, param) computes the next step of a
% BMGS-SVL-Arnoldi or BMGS-LTS-Arnoldi basis for A and B with the inner
% product specified by param.inner_prod.  When `param.new_cycle = true`,
% the basis is continued from a previously stored basis in param.
% param.skel indicates whether SVL or LTS is used.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract reused values to avoid calling param repeatedly
ip = param.inner_prod;

if param.new_cycle  % Start the basis from scratch
    % Initialize storage and indices
    V = zeros(n,2*s);
    k = 1;
    ks = k*s;
    kk = 1:s;
    
    % Compute first basis vector (same as BMGS)
    if param.first_cycle
        [V(:,kk), Beta, T, param] = intra_ortho(B, param);
        param.Beta = Beta;
    else
        V(:,kk) = B;
    end
    
    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);

    % Initialize T_diag
    if ~param.first_cycle
        switch ip
            case 'cl'
                T = eye(s);
            case 'gl'
                T = 1;
        end
    end
    
    % Run a step of BMGS_SVL
    [V(:,kk+s), R_diag, S, T, param] = bmgs_svl_step(V(:,1:ks), W, T, k, param);

    % Build and store H
    switch ip
        case 'cl'
            H = zeros(ks+s,ks);
            H(kk+s,kk) = R_diag;
            H(1:ks,kk) = S;
            param.Hnext = H(kk+s,kk);
            param.Hshort = H(1:ks,1:ks);

        case 'gl'
            H = zeros(k+1,k);
            H(k+1,k) = R_diag;
            H(1:k,k) = S;
            param.Hnext = H(k+1,k);
            param.Hshort = H(1:k,1:k);
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

else % Continue basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.T,1)/s;
            H = [param.Hshort zeros((k-1)*s,s);...
                zeros(s,(k-2)*s) param.Hnext zeros(s)];
            T = [param.T zeros(k*s,s);
                zeros(s,(k+1)*s)];

        case 'gl'
            k = size(param.T,1);
            H = [param.Hshort zeros(k-1,1);
                zeros(1,k-2) param.Hnext 0];
            T = [param.T zeros(k,1);
                zeros(1,k+1)];
    end
    V = [param.Vshort param.Vnext zeros(n, s)];
    
    % Initialize indices
    ks = k*s;
    kk = ks-s+1:ks;

    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);
    
    % Run a step of BMGS_SVL
    [V(:,kk+s), R_diag, S, T, param] = bmgs_svl_step(V(:,1:ks), W, T, k, param);
    
    % Update and store H
    switch ip
        case 'cl'
            H(kk+s,kk) = R_diag;
            H(1:ks,kk) = S;
            param.Hnext = H(kk+s,kk);
            param.Hshort = H(1:ks,1:ks);

        case 'gl'
            H(k+1,k) = R_diag;
            H(1:k,k) = S;
            param.Hnext = H(k+1,k);
            param.Hshort = H(1:k,1:k);
    end
    
end
param.T = T;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end

function [Q, R_diag, S, T, param] = bmgs_svl_step(QQ, W, T, k, param)
% Extract parameters for readability
s = param.s;
ip = param.inner_prod;
skel = param.skel;

% Compute first entries of H and next basis vector
[tmp, param] = inner_prod(QQ, W, param);

% Initialize indices
ks = k*s;
kk = ks-s+1:ks;

switch ip
    case 'cl'
        % Update H and compute next basis vector
        switch skel
            case 'bmgs_svl'
                S = T(1:ks,1:ks)' * tmp;
            case 'bmgs_lts'
                S = T(1:ks,1:ks)' \ tmp;
        end

        [tmp_vec, param] = basis_eval(QQ, S, param);
        W = W - tmp_vec;
        [Q, R_diag, T(kk+s,kk+s), param] = intra_ortho(W, param);

        % Update T
        [tmp, param] = inner_prod(QQ, Q, param);
        switch skel
            case 'bmgs_svl'
                T(1:ks,kk+s) = -T(1:ks,1:ks) * (tmp * T(kk+s,kk+s));
            case 'bmgs_lts'
                T(1:ks,kk+s) = tmp * T(kk+s,kk+s);
        end
    case 'gl'
        % Update H and compute next basis vector
        switch skel
            case 'bmgs_svl'
                S = T(1:k,1:k)' * tmp;
            case 'bmgs_lts'
                S = T(1:k,1:k)' \ tmp;
        end

        [tmp_vec, param] = basis_eval(QQ, S, param);
        W = W - tmp_vec;
        [Q, R_diag, T(k+1,k+1), param] = intra_ortho(W, param);

        % Update T
        [tmp, param] = inner_prod(QQ, Q, param);
        switch skel
            case 'bmgs_svl'
                T(1:k,k+1) = -T(1:k,1:k) * (tmp * T(k+1,k+1));
            case 'bmgs_lts'
                T(1:k,k+1) = tmp * T(k+1,k+1);
        end
end
end