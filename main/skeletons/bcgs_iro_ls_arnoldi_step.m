function param = bcgs_iro_ls_arnoldi_step(A, B, param)
% param = BCGS_IRO_LS_ARNOLDI_STEP(A, B, param) computes the next step of a
% BCGSI+LS-Arnoldi basis for A and B with the inner product specified by
% param.inner_prod. When `param.new_cycle = true`, the basis is continued
% from a previously stored basis in param.
%
% Adapted from https://github.com/dbielich/DCGS2.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract parameters for readability
ip = param.inner_prod;

if param.new_cycle % Start new basis from scratch
    % Initialize storage and auxiliary block vector
    switch ip
        case 'cl'
            H = zeros(2*s,s);
        case 'gl'
            H = zeros(2,1);
    end
    V = zeros(n, 2*s);

    % First step
    if param.first_cycle
        [V(:,1:s), Beta, ~, param] = intra_ortho(B, param);
        param.Beta = Beta;
    else
        V(:,1:s) = B;
    end

    % Initialize auxiliary vector
    U = V(:,1:s);
    for k = 2:3
        % Increment block index
        ks = k*s;
    
        % Compute next vector to orthonormalize
        [W, param] = matvec(A, U, param);
    
        if k == 2
            [J, param] = inner_prod(U, W, param);

            switch ip
                case 'cl'
                    H(1:s,1:s) = J;
                case 'gl'
                    H(1,1) = J;
            end

            % Set up auxiliary block vector for next iteration
            [tmp_vec, param] = basis_eval(V(:,1:s), J, param);
            U = W - tmp_vec;
            
        elseif k == 3
            % Run a step of BCGS_IRO_BL
            [H, J, U, V, param] = bcgs_iro_bl_step(H, J, U, V, W, k, param);
        end
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;

else % Continue basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.Hshort,1)/s;
            H = [param.Hshort zeros(k*s,s);...
                zeros(s,(k-1)*s) param.Hnext zeros(s);...
                zeros(s,(k+1)*s)];
        case 'gl'
            k = size(param.Hshort,1);
            H = [param.Hshort zeros(k,1);...
                zeros(1,k-1) param.Hnext 0;...
                zeros(1,k+1)];
    end
    J = param.J;
    U = param.U;
    V = [param.Vshort param.Vnext zeros(n,s)];
    
    % Increment block index
    k = k+3;
    ks = k*s;
    
    % Compute next vector to orthonormalize
    [W, param] = matvec(A, U, param);
    
    % Run a step of BCGS_IRO_BL
    [H, J, U, V, param] = bcgs_iro_bl_step(H, J, U, V, W, k, param);
end

% Assign outputs
switch ip
    case 'cl'
        param.Hnext = H(ks-2*s+1:ks-s,ks-3*s+1:ks-2*s);
        param.Hshort = H(1:ks-2*s,1:ks-2*s);

    case 'gl'
        param.Hnext = H(k-1,k-2);
        param.Hshort = H(1:k-2,1:k-2);
        
end
param.J = J;
param.U = U;
param.Vnext = V(:,ks-2*s+1:ks-s);
param.Vshort = V(:,1:ks-2*s);
end

function [H, J, U, V, param] = bcgs_iro_bl_step(H, J, U, V, W, k, param)
% Extract reusable quantities
s = param.s;
ip = param.inner_prod;

% Set up block indices
s1 = 1:s;
s2 = s+1:2*s;
kk = (k-1)*s+1:k*s;
ks = k*s;

% Compute temporary quantities -- the only sync point!
[tmp_prod, param] = inner_prod([V(:,1:ks-2*s) U], [U W], param);
switch ip
    case 'cl'
        Y = tmp_prod(1:end-s,s1);
        Z = tmp_prod(1:end-s,s2);
        
        % Pythagorean trick for R diagonal
        Om = tmp_prod(end-s+1:end,s1) - Y' * Y;       % local inner product
        [R_diag, param.nan_flag] = chol_nan(Om);

        % Assign new entries to H
        H(kk-s,kk-2*s) = R_diag;
        H(1:ks-2*s,kk-2*s) = J + Y;
        
        % Set up small auxiliary vector J
        P = R_diag' \ (tmp_prod(end-s+1:end,s2) - Y' * Z);       % local inner product
        J = ([Z; P] - H(1:ks-s,1:ks-2*s) * Y ) / R_diag;

    case 'gl'
        Y = tmp_prod(1:end-1,1);
        Z = tmp_prod(1:end-1,2);
        
        % Pythagorean trick for R diagonal
        Om = tmp_prod(end,1) - Y' * Y;        % local inner product
        R_diag = sqrt(Om);

        % Assign new entries to H
        H(k-1,k-2) = R_diag;
        H(1:k-2,k-2) = J + Y;
        
        % Set up small auxiliary vector J
        P = R_diag' \ (tmp_prod(end,2) - Y' * Z);        % local inner product
        J = ([Z; P] - H(1:k-1,1:k-2) * Y ) / R_diag;
end
% Warning: do not try to combine basis_eval calls here-- Y tends to have
% very small magnitude, and combining with [Z; P] throws away precision
% Finish normalizing last V
[tmp_vec, param] = basis_eval(V(:,1:ks-2*s), Y, param);
V(:,kk-s) = (U - tmp_vec) / R_diag;

% Finish setting up auxiliary vector U
[tmp_vec, param] = basis_eval(V(:,1:ks-s), [Z; P], param);
U = (W - tmp_vec) / R_diag;
end 