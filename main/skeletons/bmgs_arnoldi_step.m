function param = bmgs_arnoldi_step(A, B, param)
% param = BMGS_ARNOLDI_STEP(A, B, param) computes the next step of a
% BMGS-Arnoldi basis for A and B with a given inner product
% (param.inner_prod) and muscle (param.musc).  When `param.new_cycle =
% true`, the basis is continued from a previously stored basis in param.

%%
% Extract dimensions
s = param.s;
n = size(B,1);

% Extract reused values to avoid calling param repeatedly
ip = param.inner_prod;

if param.new_cycle % Start the basis from scratch
    % Initialize storage and indices
    V = zeros(n,2*s);
    k = 1;
    kk = 1:k*s;
    
    % Compute first basis vector
    if param.first_cycle
        [V(:,kk), Beta, ~, param] = intra_ortho(B, param);
        param.Beta = Beta;
    else
        V(:,kk) = B;
    end
    
    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);

    % Compute first entries of H and next basis vector
    switch ip
        case 'cl'
            H = zeros(2*s,s);
            [H(kk,kk), param] = inner_prod(V(:,kk), W, param);
            W = W - V(:,kk) * H(kk,kk);
            [V(:,kk+s), H(kk+s,kk), ~, param] = intra_ortho(W, param);

            param.Hnext = H(end-s+1:end,end-s+1:end);
            param.Hshort = H(1:end-s,:);

        case 'gl'
            H = zeros(k+1,k);
            [H(k,k), param] = inner_prod(V(:,kk), W, param);
            W = W - V(:,kk) * H(k,k);
            [V(:,kk+s), H(k+1,k), ~, param] = intra_ortho(W, param);

            param.Hnext = H(end,end);
            param.Hshort = H(1:end-1,:);
    end
    
    % Reset flag for continuing basis
    param.new_cycle = false;
    
else % Continue the basis
    % Extract current basis
    switch ip
        case 'cl'
            k = size(param.Hshort,1)/s;
            H = [param.Hshort zeros(k*s,s);...
                zeros(s,k*s-s) param.Hnext zeros(s)];

        case 'gl'
            k = size(param.Hshort,1);
            H = [param.Hshort zeros(k,1);
                zeros(1,k-1) param.Hnext 0];
    end
    V = [param.Vshort param.Vnext zeros(n, s)];
    
    % Initialize indices
    k = k+1;
    ks = k*s;
    kk = ks-s+1:ks;

    % Compute next vector
    [W, param] = matvec(A, V(:,kk), param);
    
    % BMGS step
    jj = 1:s;
    switch ip
        case 'cl'
            for j = 1:k
                [H(jj,kk), param] = inner_prod(V(:,jj), W, param);
                W = W - V(:,jj) * H(jj,kk);
                jj = jj+s;
            end
            [V(:,kk+s), H(kk+s,kk), ~, param] = intra_ortho(W, param);

            param.Hnext = H(end-s+1:end,end-s+1:end);
            param.Hshort = H(1:end-s,:);

        case 'gl'
            for j = 1:k
                [H(j,k), param] = inner_prod(V(:,jj), W, param);
                W = W - V(:,jj) * H(j,k);
                jj = jj + s;
            end
            [V(:,kk+s), H(k+1,k), ~, param] = intra_ortho(W, param);

            param.Hnext = H(end,end);
            param.Hshort = H(1:end-1,:);
    end
end

param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
end