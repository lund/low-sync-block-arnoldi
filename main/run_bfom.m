function [run_data, param] = run_bfom(A, B, param, config)
% [run_data, param] = run_BFOM(A, B, param, config) solves A X = B with
% variants of BFOM specified by the parameters in struct param and returns
% a struct run_data with various performance measures. config is a string
% denoting which of the two configurations to run ('time', 'stab', or
% 'both'); the default is 'both'
%
% Accepted param fields include:
%   - .conv_check: 'approx' or 'true'
%   - .true_sol: true solution A\B
%   - .err_scale: scalar (usually norma of true solution or approximate)
%      by which to scale the error measure (which is the residual if
%      true_sol is not given)
%   - .inner_prod: inner products
%   - .max_cycles: maximum number of restart cycles
%   - .max_iterations: maximum basis size before restarting
%   - .max_threads: maximum number of CPU threads
%   - .mod: 'none'/'fom', 'harmonic'/'gmres' ('radau-lanczos' to be added
%      later)
%   - .musc: muscles
%   - .n: dimension of the operator A
%   - .num_runs: number of times to run each ip-skel-musc configuration
%      (average is taken for timings)
%   - .skel: skeletons
%   - .tol: desired accuracy
%   - .verbose: 0 (bare minimum print-outs); 1 (just error); 2 (error and
%      loss of orthogonality [LOO])
%
% Output fields of run_data include:
%   - .Acounts: vector of A-call counts per algorithm
%   - .alg_config: a cell of strings denoting the executed algorithm
%      configurations
%   - .basis_eval_count: vector of basis_eval counts per algorithm
%   - .cond_BAV: a cell of cells containing iteration-wise computations of
%      cond([B A*V]), where V is the Krylov basis
%   - .loss_ortho: a cell of cells containing iteration-wise loss of
%      orthogonality data for each cycle
%   - .approx_res: a cell of vectors containing approximate residuals per
%      iteration
%   - .run_times: vector of (average) run times for each algorithm
%      configuration
%   - .sync_counts = vector of sync counts per algorithm
%   - .total_cycles: vector of total number of cycles to converge for
%      each algorithm
%   - .total_cycles: vector of total number of iterations to converge for
%      each algorithm
%
% run_data = run_bfom without arguments returns results for a default
% problem where A is a 100 x 100 tridiagonal matrix and B a 100 x 2
% right-hand side.

%%
addpath(genpath('../../'))

% Initialize param and define operator and RHS
if nargin == 0
    n = 100;
    param = [];
    param.n = n;
    
    e = ones(n,1);
    A = spdiags([e -(1:n)' e], -1:1, n, n);
    B = [e/sqrt(n) (1:n)'];
    
    config = 'both';
    
elseif nargin == 1
    n = size(A,1);
    param = [];
    param.n = n;
    
    e = ones(n,1);
    B = [e/sqrt(n) (1:n)'];
    
    config = 'both';
    
elseif nargin == 2
    n = 100;
    param = [];
    param.n = n;
    
    config = 'both';
    
elseif nargin == 3
    if isfield(param,'n')
        n = param.n;
    else
        n = 100;
    end
    
    config = 'both';
end
if isempty(A)
    if isempty(param)
        n = 100;
        param = [];
        param.n = n;
    elseif isfield(param,'n')
        n = param.n;
    else
        n = 100;
        param.n = 100;
    end
    e = ones(n,1);
    A = spdiags([e -(1:n)' e], -1:1, n, n);
end
if isempty(B)
    n = param.n;
    e = ones(n,1);
    B = [e/sqrt(n) (1:n)'];
end
s = size(B,2);
param.s = s;

% Convert config into flags
if strcmp(config,'time') || strcmp(config,'both')
    time_flag = true;
else
    time_flag = false;
end
if strcmp(config,'stab') || strcmp(config,'both')
    stab_flag = true;
else
    stab_flag = false;
end

% Extract parameters and set defaults
if isfield(param, 'conv_check')
    conv_check = param.conv_check;
else
    conv_check = 'true';
    param.conv_check = conv_check;
end
if strcmp(param.conv_check, 'true')
    true_flag = true;
    if isfield(param, 'true_sol')
        true_sol = param.true_sol;
    else
        true_sol = A\B;
        param.true_sol = true_sol;
    end
    if isfield(param, 'err_scale')
        err_scale = param.err_scale;
    else
        err_scale = norm(true_sol,'fro');
        param.err_scale = err_scale;
    end
else
    true_flag = false;
end
if isfield(param, 'inner_prod')
    ip = param.inner_prod;
else
    ip = {'cl', 'gl'};
    param.ip = ip;
end
if isfield(param, 'max_cycles')
    max_cycles = param.max_cycles;
else
    max_cycles = 300;
    param.max_cycles = max_cycles;
end
if isfield(param, 'max_iterations')
    max_iterations = param.max_iterations;
else
    max_iterations = 10;
    param.max_iterations = max_iterations;
end
if isfield(param, 'max_threads')
    max_threads = param.max_threads;
else
    max_threads = 4;
    param.max_threads = max_threads;
end
maxNumCompThreads(max_threads);
fprintf('Warning: Max number of compute threads set to %d.\n', max_threads)
if isfield(param, 'mod')
    mod = param.mod;
else
    mod = {'fom', 'gmres'};
    param.mod = mod;
end
if isfield(param, 'musc')
    musc = param.musc;
else
    musc = {'houseqr'};
    param.musc = musc;
end
if isfield(param,'norm')
    norm_str = param.norm;
else
    norm_str = 'fro';
    param.norm = norm_str;
end
if isfield(param, 'num_runs')
    num_runs = param.num_runs;
else
    num_runs = 1;
    param.num_runs = num_runs;
end
if isfield(param, 'skel')
    skel = param.skel;
else
    skel = {'bmgs', 'bcgs_pip', 'bcgs_pio', 'bmgs_svl', 'bmgs_lts',...
    'bmgs_cwy', 'bmgs_icwy', 'bcgs_iro_ls'};
    param.skel = skel;
end
store_cond_BAV = stab_flag;
param.store_cond_BAV = store_cond_BAV;
store_loss_ortho = stab_flag;
param.store_loss_ortho = store_loss_ortho;
if isfield(param, 'tol')
    tol = param.tol;
else
    tol = 1e-6;
    param.tol = tol;
end
if isfield(param, 'verbose')
    verbose = param.verbose;
else
    verbose = 0;
    param.verbose = verbose;
end

% Print key parameters
fprintf('n = %d, s = %d, m = %d, max cycles = %d\n',...
    n, s, max_iterations, max_cycles)

% Convert single arguments to cells
if ischar(ip)
    ip = {ip};
end
if ischar(mod)
    mod = {mod};
end
if ischar(skel)
    skel = {skel};
end
if ischar(musc)
    musc = {musc};
end

% Create par_param to facilitate parallelization
par_param = create_par_param(ip, mod, skel, musc);
num_alg = length(par_param.ip);

% Initialize storage for outputs
alg_config = cell(num_alg,1);

if time_flag
    Acounts = zeros(num_alg,1);
    basis_eval_counts = zeros(num_alg,1);
    run_times_cum = zeros(num_alg,1);
    run_times = cell(num_alg,1);
    sync_counts = zeros(num_alg,1);
    total_cycles = zeros(num_alg,1);
    total_iterations = zeros(num_alg,1);
end
if stab_flag
    approx_res = cell(num_alg,1);
    cond_BAV = cell(num_alg,1);
    true_err = cell(num_alg,1);
    loss_ortho = cell(num_alg,1);
end

% Ignore 'Matrix is close to singular or badly scaled' -- WE KNOW ALREADY
% AND WE DON'T GIVE A DAMN
warning('off');

% Run through tests
for a = 1:num_alg
    rt = zeros(num_runs,1);
    for r = 1:num_runs
        % Parameter set-up
        qaram = [];
        qaram.conv_check = conv_check;
        if true_flag
            qaram.err_scale = err_scale;
            qaram.true_sol = true_sol;
        end
        qaram.inner_prod = par_param.ip{a};
        qaram.max_cycles = max_cycles;
        qaram.max_iterations = max_iterations;
        qaram.mod = par_param.mod{a};
        qaram.musc = par_param.musc{a};
        qaram.norm = norm_str;
        qaram.s = s;
        qaram.skel = par_param.skel{a};
        qaram.store_cond_BAV = store_cond_BAV;
        qaram.store_loss_ortho = store_loss_ortho;
        qaram.tol = tol;
        qaram.verbose = verbose;
    
        qaram = bfom(A, B, qaram);
        if qaram.flag == 1 || qaram.flag == 3 || qaram.flag == 5
            rt(r) = sum(cell2mat(qaram.run_times));
        else
            break
        end
    end

    % Save for print out
    if qaram.flag == 1 || qaram.flag == 3 || qaram.flag == 5
        alg_config{a} = sprintf('%s-%s(%s)-%s',...
            qaram.inner_prod, qaram.skel, qaram.musc,...
            qaram.mod);
        Acounts(a) = qaram.Acount;
        basis_eval_counts(a) = qaram.basis_eval_count;
        if time_flag
            run_times_cum(a) = mean(rt);
            run_times{a} = qaram.run_times;
            sync_counts(a) = qaram.sync_count;
            total_cycles(a) = length(qaram.approx_res);
            total_iterations(a) = length(cell2mat(qaram.approx_res));
        end
        if stab_flag
            approx_res{a} = qaram.approx_res;
            cond_BAV{a} = qaram.cond_BAV;
            if true_flag
                true_err{a} = qaram.true_err;
            end
            loss_ortho{a} = qaram.loss_ortho;
        end
    end
end

% Save non-empty outputs
ind = Acounts~=0;
run_data.Acounts = Acounts(ind);
run_data.basis_eval_counts = basis_eval_counts(ind);
run_data.alg_config = alg_config(ind);
run_data.approx_res = approx_res(ind);
run_data.true_err = true_err(ind);
if time_flag
    run_data.run_times_cum = run_times_cum(ind);
    run_data.run_times = run_times(ind);
    run_data.sync_counts = sync_counts(ind);
    run_data.total_cycles = total_cycles(ind);
    run_data.total_iterations = total_iterations(ind);
end
if store_cond_BAV
    run_data.cond_BAV = cond_BAV(ind);
end
if store_loss_ortho
    run_data.loss_ortho = loss_ortho(ind);
end
end