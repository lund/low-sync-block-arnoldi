% Script for generating results in paper
% Used interactive node on Mechthild: sinteractive -N 1 -c 16 -p medium 

multithread_bcgs_p;

disp('tridiag(1000,70)')
tridiag(1000,70);

disp('power_system')
power_system;

id_str = {'Bomhof/circuit_2',...
    'Rajat/rajat03',...
    'MathWorks/Kaufhold',...
    'TKK/t2d_q9'};
for i = 1:length(id_str)
    disp(id_str{i})
    suite_sparse(id_str{i}, 10, 16);
end

disp('lapl_2d')
lapl_2d;

fprintf(['All results can be found in the results folder.\n',...
    'Compile figures.tex for a nice report, or load\n',...
    'run_data.mat to further analyze the raw data.\n'])