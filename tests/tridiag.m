function [run_data, param] = tridiag(n,m)
% TRIDIAG runs a unit test for RUN_BFOM.
%
% For additional defaults, see RUN_BFOM.

%%
addpath(genpath('../../'))
if nargin == 0
    n = 1000;
    m = 70;
elseif nargin == 1
    m = ceil(sqrt(n));
end

e = ones(n,1);
B = [e/sqrt(n) (1:n)'];
A = spdiags([e -(1:n)' e], -1:1, n, n);
true_sol = A \ B;

% Set basic parameters
param.true_sol = true_sol;
param.inner_prod = {'cl', 'gl'};
param.max_cycles = 100;
param.max_iterations = m;
param.max_threads = 16;
param.mod = {'fom'};
param.musc = {'cholqr'};
param.n = n;
param.num_runs = 5;
param.save_str = sprintf('%s/results/%s_n%d_m%d', pwd, mfilename, n, m);
param.skel = {'bcgs_pip', 'bcgs_iro_ls'...
    'bmgs_cwy', 'bmgs_icwy', 'bmgs_svl', 'bmgs_lts', 'bmgs'};
param.test_id = mfilename;
param.tol = 1e-10;
param.verbose = 0;

% Get timings and stability data
[run_data, param] = run_bfom(A, B, param);

% Clean up data
run_data = analyze_bfom(run_data, param);

% Plot data
visualize_bfom(run_data, param);

% TeX data
tex_bfom(run_data, param)
end