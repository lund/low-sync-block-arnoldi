% POWER_NETWORK runs multiple solvers for A X = B, where A is given by the
% HB/1138 example from the Suite-Sparse Collection
% (https://suitesparse-collection-website.herokuapp.com/) and m = 30. B is
% a randomly generated block vector with 5 columns.
%
% See also SUITE_SPARSE.

%%
addpath(genpath('../../'))

id = 'HB/1138_bus';
m = 30;
max_threads = 16;

% Create directory for saving outputs
data_str = sprintf('%s/data',pwd);
if ~exist(data_str, 'dir')
    mkdir(data_str);
end

url_str = sprintf(...
    'https://suitesparse-collection-website.herokuapp.com/mat/%s.mat',id);

% Extract name of file
split_id = split(id,'/');
id = split_id{end};

% Load data and prepare for saving files
load_str = sprintf('data/%s.mat',id);
if isfile(load_str)
    % Then load what was downloaded and saved during a previous run
    data = load(load_str);
    B = data.B;
    A = data.A;
    true_sol = data.true_sol;
else
    fprintf('The missing .mat file will be downloaded from the Suite Sparse Collection.\n');
    data = load(websave(load_str,url_str));
    A = data.Problem.A;
    n = size(A,1);
    B = rand(n,5);
    fprintf('Please wait while the true solution is being computed with backslash.\n')
    true_sol = A\B;
    save(load_str,'A','B','true_sol');
end
clear data

n = size(B,1);

% Set up preconditioning
options.type = 'nofill';
[L, U] = ilu(A, options);
LAU = @(y) L \ (A * (U \ y));

param = [];
param.conv_check = 'true';
true_sol = U * true_sol;
param.true_sol = true_sol;
param.error_scale = norm(true_sol,'fro');
param.inner_prod = {'cl'};
param.max_cycles = 100;
param.max_iterations = m;
param.max_threads = max_threads;
param.mod = {'gmres'};
param.musc = {'cholqr'};
param.n = n;
param.num_runs = 5;
param.save_str = sprintf('%s/results/%s',pwd,id);
param.skel = {'bmgs', 'bcgs_pip', 'bmgs_cwy', 'bmgs_icwy', 'bcgs_iro_ls'};
param.test_id = id;
param.tol = 1e-6;
param.verbose = 0;

% Get timings and stability data
[run_data, param] = run_bfom(LAU, L \ B, param);

% Clean up data
run_data = analyze_bfom(run_data, param);

% Plot data
visualize_bfom(run_data, param);

% TeX data
tex_bfom(run_data, param)