% LAPL_2D runs multiple solvers for A X = B, where A is the second-ordered
% centered finite differences stencil of the 2D Laplacian, and B is a
% normalized tall-skinny matrix of right-hand-sides.

%%
addpath(genpath('../../'))

% Create directory for saving outputs
data_str = sprintf('%s/data',pwd);
if ~exist(data_str, 'dir')
    mkdir(data_str);
end

% Load data and prepare for saving files
load_str = 'data/lapl_2d_exact_sol.mat';
if isfile(load_str)
    % Then load what was downloaded and saved during a previous run
    data = load(load_str);
    B = data.B;
    A = data.A;
    true_sol = data.true_sol;
else
    fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
    data = load(websave(load_str,...
        'https://zenodo.org/record/6414224/files/lapl_2d_exact_sol.mat?download=1'));
    B = data.B1;
    A = data.A;
    true_sol = A\B;
    save(load_str,'A','B','true_sol');
end
clear data

param = [];
param.conv_check = 'true';
param.true_sol = true_sol;
param.error_scale = norm(true_sol,'fro');
param.inner_prod = {'cl', 'gl'};
param.max_cycles = 500;
param.max_iterations = 25;
param.max_threads = 16;
param.mod = {'fom'};
param.musc = {'cholqr'};
param.n = size(B,1);
param.num_runs = 5;
param.save_str = sprintf('%s/results/%s',pwd,mfilename);
param.skel = {'bmgs', 'bcgs_pip', 'bmgs_svl', 'bmgs_lts',...
    'bmgs_cwy', 'bmgs_icwy', 'bcgs_iro_ls'};
param.test_id = mfilename;
param.tol = 1e-6;
param.verbose = 0;

% Get timings and stability data
[run_data, param] = run_bfom(A, B, param);

% Clean up data
run_data = analyze_bfom(run_data, param);

% Plot data
visualize_bfom(run_data, param);

% TeX data
tex_bfom(run_data, param)