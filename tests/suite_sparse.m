function suite_sparse(id, max_iters, max_threads, max_cycles)
% SUITE_SPARSE(id, m, max_threads) runs multiple solvers for A X = B, where
% A is specified by an id for a matrix stored in the Suite-Sparse
% Collection (https://suitesparse-collection-website.herokuapp.com/) and m
% is the max basis size.  B is a randomly generated block vector with 5
% columns.
%
% Running SUITE_SPARSE without argument downloads the HB/494_bus matrix.

%%
addpath(genpath('../../'))

if nargin == 0
    id = 'HB/1138_bus';
    max_iters = 10;
    max_threads = 16;
    max_cycles = 100;
elseif nargin == 1
    max_iters = 10;
    max_threads = 16;
    max_cycles = 100;
elseif nargin == 2
    max_threads = 16;
    max_cycles = 100;
elseif nargin == 3
    max_cycles = 100;
end

% Create directory for saving outputs
data_str = sprintf('%s/data',pwd);
if ~exist(data_str, 'dir')
    mkdir(data_str);
end

url_str = sprintf(...
    'https://suitesparse-collection-website.herokuapp.com/mat/%s.mat',id);

% Extract name of file
split_id = split(id,'/');
id = split_id{end};

% Load data and prepare for saving files
load_str = sprintf('data/%s.mat',id);
if isfile(load_str)
    % Then load what was downloaded and saved during a previous run
    data = load(load_str);
    B = data.B;
    A = data.A;
    true_sol = data.true_sol;
else
    fprintf('The missing .mat file will be downloaded from the Suite Sparse Collection.\n');
    data = load(websave(load_str,url_str));
    A = data.Problem.A;
    n = size(A,1);
    B = rand(n,5);
    fprintf('Please wait while the true solution is being computed with backslash.\n')
    true_sol = A\B;
    save(load_str,'A','B','true_sol');
end
clear data

n = size(B,1);

% Set up preconditioning
options.type = 'nofill';
[L, U] = ilu(A, options);
LAU = @(y) L \ (A * (U \ y));

param = [];
param.conv_check = 'true';
true_sol = U * true_sol;
param.true_sol = true_sol;
param.error_scale = norm(true_sol,'fro');
param.inner_prod = {'cl', 'gl'};
param.max_cycles = max_cycles;
param.max_iterations = max_iters;
param.max_threads = max_threads;
param.mod = {'gmres'};
param.musc = {'cholqr'};
param.n = n;
param.num_runs = 5;
param.save_str = sprintf('%s/results/%s',pwd,id);
param.skel = {'bmgs', 'bcgs_pip', 'bmgs_cwy', 'bmgs_icwy', 'bcgs_iro_ls'};
param.test_id = id;
param.tol = 1e-6;
param.verbose = 0;

% Get timings and stability data
[run_data, param] = run_bfom(LAU, L \ B, param);

% Clean up data
run_data = analyze_bfom(run_data, param);

% Plot data
visualize_bfom(run_data, param);

% TeX data
tex_bfom(run_data, param)