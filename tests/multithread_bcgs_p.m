% MWE to reproduce "unstable" Cholesky behavior in BCGS-PIP-Arnoldi
mkdir(sprintf('results/%s',mfilename))
addpath(genpath('../main/auxiliary'))

n = 100; % problem size
m = 50; % basis size (restart cycle length)
e = ones(n,1);
A = spdiags([e -(1:n)' e],-1:1,n,n);
B = [ones(n,1)/sqrt(n) (1:n)'];
s = size(B,2);
mNCT = [1 2 4 8 16];
skel = {'bcgs_pip', 'bcgs_pio'};

% For plots
symb = {'o','s','^','x', 'd'};
xstr = 'Iteration Index';
ystr = {'Loss of Orthogonality', 'cond(X)'};
skel_str = {'BCGS-PIP', 'BCGS-PIO'};
save_str = {'loo', 'cond'};
ylim = {[eps, 1e1], [1, 1e20]};

for i = 1:length(skel)
    legend_str = cell(1,length(mNCT));
    for j = 1:length(mNCT)
        % Toggle number of cores for multithreading
        maxNumCompThreads(mNCT(j));
        legend_str{j} = sprintf('maxNumCompThreads = %d\n',mNCT(j));

        % Storage for H and V
        H = zeros((m+1)*s,m*s);
        V = zeros(n,(m+1)*s);
        X = V;

        kk = 1:s;

        % Store X for computing condition number at the end
        X(:,kk) = B;

        % Compute first basis vector
        [V(:,kk), Beta] = qr(B, 0);

        LOO = NaN(1,m);
        condX = NaN(1,m);
        for k = 1:m
            % Compute next block vector
            W = A*V(:,kk);

            % Store X for computing condition number at the end
            X(:,kk+s) = W;

            % Orthogonalize Q wrt exisiting basis
            [Q, R, S, nan_flag] = bgs_step(V(:,1:k*s), W, skel{i});

            % Update Arnoldi decomposition
            H(1:k*s,kk) = S;
            H(kk+s,kk) = R;
            V(:,kk+s) = Q;
            kk = kk + s;

            % Compute LOO and cond(X)
            if nan_flag
                break
            else
                LOO(k) = norm( eye(k*s) - V(:, 1:k*s)' * V(:, 1:k*s) );
                condX(k) = cond(X(:,1:k*s));
            end
        end
        if j == 1
            % Initialize LOO plot
            fig{1} = figure('Visible','off'); ax{1} = gca; hold on;
            
            % Initialize cond(X) plot
            fig{2} = figure('Visible','off'); ax{2} = gca; hold on;
        end
        plot(ax{1}, 1:k, LOO(1:k), 'Marker', symb{j});
        plot(ax{2}, 1:k, condX(1:k), 'Marker', symb{j});
    end
    hold off;
    for a = 1:2
        set(ax{a}, 'YScale', 'log', 'XGrid', 'on', 'XMinorGrid', 'off',...
            'YGrid', 'on', 'YMinorGrid', 'off', 'FontSize', 16, ...
            'YLim', ylim{a});
        xlabel(ax{a}, xstr); ylabel(ax{a}, ystr{a});
        legend(ax{a}, legend_str{:}, 'Location', 'Best');
        title(ax{a}, skel_str{i})
        file_str = sprintf('results/%s/%s_%s',...
            mfilename, skel{i}, save_str{a});
        saveas(fig{a}, file_str);
        saveas(fig{a}, file_str, 'epsc');
    end
end
fprintf('Figures saved in results/multithread_bcgs_p. Use openfig and ''visible'' tag to view plots.\n')

%% BGS step function
function [Q, R, S, nan_flag] = bgs_step(QQ, W, skel)
[n, s] = size(W);
switch skel
    case 'bcgs_pip'
        % One step of BCGS_PIP
        tmp = [QQ W]' * W;
        S = tmp(1:end-s,:);
        Om = tmp(end-s+1:end,:) - S'*S;    
        
    case 'bcgs_pio'
        % One step of BCGS_PIO
        S = QQ' * W;
        [~, tmp] = qr([W zeros(n,s); zeros(size(S)) S], 0);
        tmp = tmp' * tmp;
        Om = tmp(1:s,1:s) - tmp(end-s+1:end, end-s+1:end);
end
[R, nan_flag] = chol_nan(Om);
Q = (W - QQ * S) / R;
end